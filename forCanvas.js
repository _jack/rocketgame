(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 800,
	height: 560,
	fps: 60,
	color: "#180732",
	opacity: 0.00,
	manifest: []
};



lib.ssMetadata = [];


// symbols:



(lib.fire2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(54,45,23,0.22)").s().p("AgFAEIgLgIIAEAAIABAAIAcACQgSACAAAEIgEAAg");
	this.shape.setTransform(-60.7,10.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(60,50,25,0.243)").s().p("AAAAEIgDAAIgKAAIAAAAIAAAAIAAAAIAAgFIAZAFIgMAAgAAFgEIAJAAIAAAEIgJgEg");
	this.shape_1.setTransform(-55.5,10.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(71,59,29,0.286)").s().p("AAAAEIgLgIIAGAAIAEABQADACAJAFIgLAAg");
	this.shape_2.setTransform(-43.4,10.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(84,70,35,0.337)").s().p("AgGAFQgFgJADABIASAAIAAAIIgQAAg");
	this.shape_3.setTransform(-34.1,7.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(48,40,20,0.196)").s().p("AAAAJQgEgEgDgFQgCgCASACIAAAJIgJAAgAAAgJIAJAEIAAABIAAABQgLgFACgBg");
	this.shape_4.setTransform(-14.9,3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(55,46,23,0.224)").s().p("AAAAAQgLgDADAAIAHAAIABAAIAAABIAdAHIgdgFgAgSAFIgKAAIAAgIIAcADIAAAFIgSAAg");
	this.shape_5.setTransform(-25,6.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(252,210,106,0.988)").s().p("AikBbQAYAPANARQgvgXgIAAIgKAAIgJAAIAJAFQAAADAUANIgWgLIgbgIIAAAIIAAAAIAAAAIAAAAIgWgNIgBADIADACIgBAAIgegCIAAAAIgEAAIg1APIgBg9QgChIAAhGIAAgKIAYgEIAXABQBHgRAlgQIgSAaIADADIAqgVIglAfQgUAfAPgRQAQgRAYAAQAFAAAogPQApgQgIAJIgMAQQgFAGAJAAIAKAAQAIAAAagPIgsAcIBNgMQgdASgtAWQA5gQABgFQAEAAAwgTQgeAXgBAHQgCAFARgCIAEgBIAcgDQAEAAAggNQheAqAhgGIA7gKIAZgDQAGAAATgIQgQAOgJAPQgIAQA6gQQA6gQgZAPIgiAXIALgBQAIABAMgDIAKAAQAPAAAggPQgQAUABABQATAEATAAIAKAAQAGAAAygKQguAPAOAGQgXgDgJAEQgBABAQASQgWgNgFAAIgTAAIgKAAQgUAAgUAEIATAJIAUAIIgUgIIgKgDQgCAAAMAFIAAADQgagIgEAAQgPAAgNAEQgCABA8AfQhAgagFAAQg9gVAWAPQAXAQAGAAQgBAFAVAFIgKAAIgVAAIgKAAQgOAAgOAFIAQAFIgIAAQgCAAAMADIAAACIgdgFQgVAAgSAFQgCAABLAYQhPgTgGAAIgSAAQgDAAAFAKIgCAAQgFAAAlAeIg/gqIAOAVIhQgqQAzAdgCALIAAADQgBAFAcAKIg0gNIgwgbIAZAjIAMAKIgCAAIAeAYg");
	this.shape_6.setTransform(-34.7,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68,-13.5,66.6,27);


(lib.fire1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(248,88,63,0.976)").s().p("AhSA/IgigCIgYgDQgKgBgHAAQgIgBgJACQgJADgKAFQgLAFAEgEIABh6IAAgKQAFAAgFgBQgFAABBABIAKAAQAoAAArgGQhLANAqACQAqACAqgKQgTAJAEgBQg5AJAIADQAHACAngBIAqgCQAFAAAZgLQgGAFgoANQglANALACQALACBbgOQglAOAJAGQAKAEAagBQAagCAsgBIguAHQAeAEAzABIgkAGIAQAKQgcgJgSAAQgOAAAEAKQAEAJgGgJQgigLgJAMQgCADAnAOQg/gVgCAFQgDAEArAZQgngMgFAAIgUAAQgFAAgzgNQAVASAqANQgygIgDADQgCACAbALQglgGgKgEQgKgFgLgDIgcgNIBCAiIhYgaIBaAkQgqgJgUAAg");
	this.shape.setTransform(-20.9,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-41.1,-7.3,40.5,14.5);


(lib.text2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgrA8IAAh3IAlAAQAWAAAOAPQAOAOAAAYIAAAOQAAAXgOAPQgOAOgWAAgAgbAvIAVAAQAPAAAJgLQAKgLAAgRIAAgPQAAgRgKgLQgJgLgPAAIgVAAg");
	this.shape.setTransform(69.4,13.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAdA8Ig4hcIgBAAIAABcIgQAAIAAh3IAQAAIA4BbIABAAIAAhbIAQAAIAAB3g");
	this.shape_1.setTransform(57.3,13.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgmA8IAAh3IBMAAIAAANIg8AAIAAAnIA1AAIAAALIg1AAIAAArIA9AAIAAANg");
	this.shape_2.setTransform(46.8,13.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(39.3,0,37.5,26.4);


(lib.text1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AggCnIAAkZIheAAIAAg0ID9AAIAAA0IheAAIAAEZg");
	this.shape.setTransform(111.8,15.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ah/CnIAAlNICDAAQA6ABAhAdQAhAdAAAwQAAAwghAbQghAeg6gBIhAAAIAAB6gAg8gFIBAAAQAcAAAPgOQAOgQAAgYQAAgXgOgQQgPgQgcAAIhAAAg");
	this.shape_1.setTransform(83.8,15.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("ABRCnIgXhIIh0AAIgWBIIhEAAIBzlNIBDAAIByFNgAgpAqIBSAAIgpiCIAAAAg");
	this.shape_2.setTransform(52.9,15.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AggCnIAAkZIheAAIAAg0ID9AAIAAA0IheAAIAAEZg");
	this.shape_3.setTransform(24.3,15.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhdCFQglgnAAg9IAAhBQAAg8AkgoQAlgnA5ABQA+AAAjAfQAjAggBA3IgBABIhBAAQAAghgQgRQgQgSghAAQgcAAgSAYQgSAZAAAmIAABBQAAAmATAZQATAXAdAAQAgAAAPgQQAPgRAAghIBBAAIAAABQABA2giAgQgiAfg8AAQg6ABgmgng");
	this.shape_4.setTransform(-3.9,15.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-21.2,-19.2,148.7,66);


(lib.lines = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#260E50").s().p("Eg2XAkVMAAAhIpIAUAAMAAABIVMBsaAAAIAAAUg");
	this.shape.setTransform(348,-232.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-465,696,465);


(lib.flamePink = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B93281").s().p("AgQARQgHgHgBgKQABgJAHgHQAIgIAIAAQALAAAGAIQAIAHAAAJQAAAKgIAHQgGAIgLAAQgIAAgIgIg");
	this.shape.setTransform(2.5,2.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,5,5);


(lib.flameBlue = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#4549C2").s().p("AgYAYQgKgKAAgOQAAgOAKgKQALgKANAAQAOAAAKAKQALAKAAAOQAAAOgLAKQgKALgOAAQgNAAgLgLg");
	this.shape.setTransform(3.5,3.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,7,7);


(lib.btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E4307A").s().p("A0iE2QhdgBhChBQhChCAAhdIAAipQAAhdBChCQBChCBdAAMApEAAAQBdAABDBCQBCBCgBBdIAACpQABBdhCBCQhDBBhdABg");
	this.shape.setTransform(154,31);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,308,62);


(lib.area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E4307A").s().p("A0gE3QhcAAhChCQhDhCAAhdIAAgkQgEgZAAgZQAAgbAEgZIAAgeQAAhdBDhCQBChCBcAAIAoAAQAUgDAVAAMAmbAAAQAVAAAUADIAwAAQBdAABBBCQBDBCAABdIAACoQAABdhDBCQhBBChdAAg");
	this.shape.setTransform(154.2,31.1);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.fireAll = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// fire-1
	this.instance = new lib.fire1();
	this.instance.setTransform(-68.3,0,1.155,1.093,0,0,0,-41.2,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-41.3,scaleX:1.09,scaleY:1.17,x:-68.4},5).to({regX:-41.2,scaleX:1.16,scaleY:1.09,x:-68.3},5).to({regX:-41.3,scaleX:1.38,scaleY:1.17},5).to({regX:-41.2,scaleX:1.16,scaleY:1.09},6).wait(1));

	// fire-2
	this.instance_1 = new lib.fire2();
	this.instance_1.setTransform(-67.8,0,1.104,1.013,0,0,0,-67.8,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regX:-34.7,regY:-0.1,scaleX:1.1,scaleY:1.02,x:-31.4,y:-0.1},0).wait(1).to({scaleX:1.09,scaleY:1.02,x:-31.7},0).wait(1).to({scaleX:1.08,scaleY:1.04,x:-32.2},0).wait(1).to({scaleX:1.05,scaleY:1.06,x:-33},0).wait(1).to({scaleX:1.03,scaleY:1.08,x:-33.8},0).wait(1).to({scaleX:1.01,scaleY:1.1,x:-34.6},0).wait(1).to({scaleX:0.99,scaleY:1.12,x:-35.3},0).wait(1).to({scaleX:0.97,scaleY:1.13,x:-35.8},0).wait(1).to({scaleX:0.96,scaleY:1.14,x:-36.1},0).wait(1).to({regX:-68,regY:0,scaleX:0.96,scaleY:1.14,x:-68,y:0},0).wait(1).to({regX:-34.7,regY:-0.1,scaleX:0.96,scaleY:1.14,x:-35.9,y:-0.1},0).wait(1).to({scaleX:0.97,scaleY:1.13,x:-35.6},0).wait(1).to({scaleX:0.98,scaleY:1.12,x:-35.2},0).wait(1).to({scaleX:1,scaleY:1.1,x:-34.6},0).wait(1).to({scaleX:1.02,scaleY:1.09,x:-33.9},0).wait(1).to({scaleX:1.05,scaleY:1.07,x:-33.1},0).wait(1).to({scaleX:1.07,scaleY:1.05,x:-32.4},0).wait(1).to({scaleX:1.08,scaleY:1.03,x:-31.7},0).wait(1).to({scaleX:1.1,scaleY:1.02,x:-31.4},0).wait(1).to({scaleX:1.1,scaleY:1.01,x:-31.1},0).wait(1).to({regX:-67.8,regY:0,scaleX:1.1,scaleY:1.01,x:-67.8,y:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68.2,-13.7,73.8,27.4);


(lib.buttonGo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{s1:1,s2:30});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		this.area.addEventListener("rollover", func_1.bind(this));
		this.area.addEventListener("rollout", func_2.bind(this));
		
		function func_1()
		{
			this.gotoAndPlay("s1");
		}
		
		function func_2()
		{
			this.gotoAndPlay("s2");
		}
	}
	this.frame_29 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(29).call(this.frame_29).wait(30));

	// area
	this.area = new lib.area();
	this.area.setTransform(154,31,1,1,0,0,0,154,31);
	new cjs.ButtonHelper(this.area, 0, 1, 2, false, new lib.area(), 3);

	this.timeline.addTween(cjs.Tween.get(this.area).wait(59));

	// mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AzNE2QiAAAhbhbQhbhbAAiAQAAh/BbhbQBbhbCAAAMAmbAAAQCAAABaBbQBcBbgBB/QABCAhcBbQhaBbiAAAg");
	mask.setTransform(154,31);

	// Слой 2
	this.instance = new lib.text1();
	this.instance.setTransform(154,31,1,1,0,0,0,50.8,13.2);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:71.8},6,cjs.Ease.get(1)).to({y:-13.4},1).to({y:35.8},8,cjs.Ease.get(1)).to({y:31},14,cjs.Ease.get(1)).to({y:71.8},6,cjs.Ease.get(1)).to({y:-13.4},1).to({y:35.8},8,cjs.Ease.get(1)).to({y:31},14,cjs.Ease.get(1)).wait(1));

	// Слой 1
	this.instance_1 = new lib.btn();
	this.instance_1.setTransform(154,31,1,1,0,0,0,154,31);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({alpha:0},29,cjs.Ease.get(1)).to({alpha:1},29,cjs.Ease.get(1)).wait(1));

	//  
	this.instance_2 = new lib.btn();
	this.instance_2.setTransform(154,31,1,1,0,0,0,154,31);
	this.instance_2.filters = [new cjs.ColorFilter(0.01, 0.01, 0.01, 1, 252.45, 90.09, 63.36, 0)];
	this.instance_2.cache(-2,-2,312,66);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(59));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,308.4,62.3);


(lib.buttonAway = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"s1":1,"s2":20});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		this.area.addEventListener("rollover", func_1.bind(this));
		this.area.addEventListener("rollout", func_2.bind(this));
		
		function func_1()
		{
			this.gotoAndPlay("s1");
		}
		
		function func_2()
		{
			this.gotoAndPlay("s2");
		}
	}
	this.frame_19 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(19).call(this.frame_19).wait(21));

	// area
	this.area = new lib.area();
	this.area.setTransform(154,31.1,0.547,0.547,0,0,0,154,31);
	new cjs.ButtonHelper(this.area, 0, 1, 2, false, new lib.area(), 3);

	this.timeline.addTween(cjs.Tween.get(this.area).wait(40));

	// mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AqfCpQhHAAgxgyQgxgxAAhGQAAhFAxgxQAxgyBHAAIU/AAQBHAAAxAyQAxAxABBFQgBBGgxAxQgxAyhHAAg");
	mask.setTransform(154,31.1);

	// Слой 3
	this.instance = new lib.text2();
	this.instance.setTransform(154.4,30.4,1,1,0,0,0,59.4,13.4);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:6.4},4,cjs.Ease.get(-1)).to({y:56.8},1,cjs.Ease.get(1)).to({y:27.1},5,cjs.Ease.get(1)).to({y:30.4},9,cjs.Ease.get(1)).wait(1).to({y:6.4},4,cjs.Ease.get(-1)).to({y:56.8},1,cjs.Ease.get(1)).to({y:27.1},5,cjs.Ease.get(1)).to({y:30.4},9,cjs.Ease.get(1)).wait(1));

	//  
	this.instance_1 = new lib.btn();
	this.instance_1.setTransform(154.2,31,0.547,0.547,0,0,0,154,31);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({alpha:0},19,cjs.Ease.get(1)).to({alpha:1},20,cjs.Ease.get(1)).wait(1));

	//  
	this.instance_2 = new lib.btn();
	this.instance_2.setTransform(154.2,31,0.547,0.547,0,0,0,154,31);
	this.instance_2.filters = [new cjs.ColorFilter(0.01, 0.01, 0.01, 1, 252.45, 90.09, 63.36, 0)];
	this.instance_2.cache(-2,-2,312,66);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(40));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(69.8,14,168.6,34.2);


(lib.rocketMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// rock.png
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#AB1E65").s().p("ABbCzQg5gIgPgkIgJAAIgRACQgEABgEAAQgcAEgbgKQgfgOABg7QABgOACgPIAAgLIAAh2IAAgKIAAgKQACgRAIghQAAAKgBgEIABAiQAAA8AFA8IAAADQAxhJAZhaIABgKIAIAAIAKAAIAKAAIAKAAIgBAGQgJA4AoAIIAIAAIACAAIAAAKIAAAeIAAALIAAAzIAAADIAAAFIAAAIQgBBcAdA7QACAEAAAFIAAAKIgKAAg");
	this.shape.setTransform(-139.1,2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BE3073").s().p("AghBYQgFg8AAg7IgBghQABAEAAgKQAGgQATgDQAHgBAGAAQAFAAAjAKIgBAKQgYBYgwBJIAAgDg");
	this.shape_1.setTransform(-144,-7.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(51,9,30,0.29)").s().p("AgLAAQAEABAEgBIARgBIgBAAQgJABgHABIgKABIACgCg");
	this.shape_2.setTransform(-139.5,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(52,74,178,0.98)").s().p("AihBuQgRgCgRAAIgWgEIgKgBIgJgBIgBAAIgEgBIgCgBQgKgCgLAAIgBAAQgFAAgCgDIADgCIAIgIIALgKIAWgXQArgyAag8IANghQABgEAAgFIAAgKQARANAhgDIAKAAIADAAQA1AKAYAAIAbACIBKAIQBdAJBKAKIAKABQgFAKgBAHQgOAPAAAHQAAgEgCACQgDADgFAKQgFAAgEAJQgCAEgUAPIgNAJIgMAIIgBABQgNAKgwAZQggASgaAIQgaAHgFAAIgIABIgIABQgJACgNgBIgNABIgeAAIgKAAQgoAAg6ADIgIgBg");
	this.shape_3.setTransform(-114.4,30.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#A59FA2").s().p("AA3BEQglgGgWgPQgkgWgUgaQgTgdgEgQIgCgHIgFgTIAYAGIAFAYIAKATQATAfAfAXQAeAXASAHIAJADIAjAJQgTgCgRgDg");
	this.shape_4.setTransform(-84.6,10.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(57,33,44,0.294)").s().p("AgBAAIACgCIADACIgBACIgEABQgBAAAAAAQgBAAAAgBQAAAAABgBQAAgBABAAg");
	this.shape_5.setTransform(-42.5,21.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(185,42,111,0.984)").s().p("Ai/DHIgKgsQgHghgDgeIAAgDIgCgaIgCghIABhYIAAgJIAAgBIAAAAIACgRIADgXIAAAAIADgTIALhLIArAKIAAAAQCdA0CMBWIALAJIASAOIgKgHIAEADIAiAZIABAAQAYAUgWAVIgZASIAAAAQg5AmhGAhQg3AahDAWIgoANQghAKgkAJIAAAAIgLADIgCgEgAjNh1IAAAAIABgWg");
	this.shape_6.setTransform(-24.4,1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CDEAF2").s().p("AgQCDQhHgDgegpQAigWAdgcQAdgaAUgXIARgaQAaglAEggIABgPIAAgJIAMAIIAFAEIAHAEIAGAEQAEACAIAKIACABIAFAHIAEAEIAHANIAAAAIABABIACAIIAGATIADAVIADAgIAAAFIgBAOIgBAIQAAAEgDAKQgDAKgBAJQgBAIgTAZQgSAZgiACIgjAEIgIABIgKgBgABnhKIAAAAIgBgBIABABg");
	this.shape_7.setTransform(-73.2,0.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#AF2F70").s().p("AgBApIgBgDIABABIgCgGQgCgIgBgaQAAgdAGghQAKA/gFAbIAAACIAAAAIgEARIAAABIgBACIAAABIAAAAIABAOIgCgXg");
	this.shape_8.setTransform(-91,-0.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#B5DFEC").s().p("AhDBsQgRgdgFgSQgFgSgCgLIgCgWQAAgKACgBIgBAAIAIgrIAHgRIACgGIAHgJQAGgHAHgGQAIgGAJgEQAIgEAdgHQAagGAfABQAeACASALIAAAJIgBAPQgEAggZAlIgUAYQgUAYgdAbQgbAcgiAWIgGgIg");
	this.shape_9.setTransform(-78.8,-2.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#B93174").s().p("AgJCrIgjgIIgJgDQgTgHgfgXQgfgXgUgiIgJgTIgBgOIAAgBIAAAAIAAgCIABgCIADgQIAAAAIABgCQAEgagKhBIAAAAQABABACgIQABgIAQgcIAGgGIAHgGQAPgLANgHQAJgGAJgCQAagIAFAAQAQgFATgCQAPgBAOACQASACATAEQAXAIAWAOQALAIAKAIQAJAJAMAQQAMAPAJAYQAGAQACAJIABAHQADAPAAAHIAAABIAAABIAAAAQABAJgDAYIgBAHIAAABIAAABIgBACIgBAEIAAAAIABAAQgEAZgJASQgFAJgHAJIgRATQgSAUgNAGQgNAFgaAHIgDABQgdAIgPABgAgziHQgcAHgJADQgJAEgHAGQgIAGgGAHIgHAJIgCAGIgHASIgHAqIAAAAQgCACABALIACAUQABAMAFASQAGASARAdIAFAHQAeApBHADIAIABIAKgBIAjgDQAigDATgZQASgZABgIQABgJADgKQADgJABgFIABgIIABgJIAAgKIgDggIgEgVIgFgTIgDgIIAAgBIAAAAIgHgNIgEgEIgGgHIgBgBQgJgKgDgCIgHgEIgGgEIgGgDIgMgJQgSgKgegCIgKAAQgYAAgXAFgAByhHIAAgBIABABgAiYhwIAEgDIgEAEg");
	this.shape_10.setTransform(-74.4,0.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("rgba(208,206,207,0.996)").s().p("ACwC5IgdAAIgKAAIgKgBIgOgCQgsgGgXgBIgJAAIgKAAIgDAAIgRAAIgIgBQhKgKhdgKIhKgHIgdgCQgYAAg1gKIgDAAIgKAAQgiADgQgNQAAgFgCgEQgdg8ABhZIAAgIIAAgCIAAgGIAAgDIAAgzIAAgKIAAgeQAVA1A4ALQBAASBBALIAaAEIAtAGQAmAEAmADIAdAAQAAg6AMghQALghAVgSIAEgDIAGgFIgHAFQgPAdgCAIQgBAIgBgBIAAAAQgIAhAAAdQABAdACAHIACAGIgBAAIABADIgZgGIAFASIACAHQAFAOATAdQAUAdAhAVQAZAPAlAHQAQADAUABIAOAAQAPgBAdgIIADgBQAagHAMgGQANgGATgTIARgTQAHgJAEgJQAKgSADgXIgBAAQAbgEAegCIAegCIAQAAIATAAIACAeIACAaIAAADQADAeAHAhIALAtIABAEQgVAHgEAAIgQACIggAFIhFAKIgXADIhQAKIgVAAgAgSisIAAAAIADgDIgDADgAGSgpIAAAAIgBACIABgCg");
	this.shape_11.setTransform(-87.7,6.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(195,54,121,0.992)").s().p("AABAxIgBAAIgHAAQgogIAJg2IABgGIAAgKQAAgEAJgLIASgGIgBACIAMgGIAFgCQANgCAKAAIACAAIAIAAIACAAIAAABIgBAEIgBAEIgFAJIgNASIgGAKIgBACQgGALADAHIAAAMQAAAFgKAiIAAgKg");
	this.shape_12.setTransform(-132.1,-14);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("rgba(253,252,253,0.996)").s().p("AEqCLIABgFIAAgBIABgBIAAgCIABgGQADgZgCgKIAAgBIAAgBIAAgBQABgGgDgPIgBgIQgCgJgGgQQgJgXgMgOQgMgQgJgIQgKgIgMgIQgVgPgXgHQgTgFgSgCQgQgBgQABQgSABgQAFQgFAAgbAIQgIADgJAFQgNAHgPAMIgFAFIgGAFIgEAEQgVASgLAeQgMAhAAA6IgdAAQgmgCgmgEIgtgGIgagFQhBgLhAgRQg4gLgVg2QAKghAAgFIAAgLQgDgHAGgNIABgCIAGgKIANgSIAFgKIABgDIAAAAIAEgBIACgBIAKgHIAGgCIAPgBIAQgCIADAAIACgBQABgBAMgCQAAAAAAgBQAAAAABAAQAAAAAAAAQABAAAAAAIADAAQAXgDAzgFIABgBIARgBIAkgEIABgBIAAAAIAygBIANgBIAQAAIAWAAIAbAAIAXAAIAFAAIAGAAIANAAIAHAAIABAAIADAAIAIAAIAUAAIAKAAIAUAAIApAAIBZAAIAUAAIAKAAIAGAAIAYAAIAKAAIAKAAIATADIAAAAIAdAEIAWADIBEAMIAKADIASAGIANAFIgLBMIgCASIAAABIgDAUIgDARIAAAAIAAACIAAAIIgBBaIgTAAIgQABIgeABQgeACgaAFgAGUBzIABgDIAAAAIgBADg");
	this.shape_13.setTransform(-88,-9.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("rgba(51,73,177,0.973)").s().p("AicBzQAJgSgBgEQgYg9gkgzQgegvgngoIAFAAIgHgBIgBAAQgBAAABAAIATgCIAvgFIAbgCIA/AAIAbAAIAKAAIAKAAIAKAAIAXACIACAAQAQACAPAEIARAEIAQAFIAfAMQAlAOAlARIAjASIgNgGQAaASAWATQAYATAUAVIAKALIAIAKIAWAWIABABIgCgBIAAAAQgGgEgDAAIAJAEIg2ABIgaAAIgXAAIgPAAIgNAAIgnAAIgLACIAAAAIgCAAIgyAFIgOACIAPgCIgDABIgBAAQgyAFgXAEIgCgBQAAAAgBAAQAAAAgBABQAAAAAAAAQAAAAAAAAQgNACgBACIgBAAIgDABIgQABIgPACIgFAAIgBABIgKAHIgDABIgDABIAAAAIABgEIAAgBIgCAAIgIAAIgCAAQgKAAgNADIgFABIgOAGIABgCg");
	this.shape_14.setTransform(-117.5,-30.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// fireAll
	this.instance = new lib.fireAll();
	this.instance.setTransform(-147.5,-0.2,1,1,-179,0,0,-68,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-221.2,-42.4,218.4,83.8);


(lib.animation = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{go:1,loop:142,away:310});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_309 = function() {
		this.gotoAndPlay("loop");
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(309).call(this.frame_309).wait(55));

	// __mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Eg2XAkUMAAAhInMBsuAAAMAAABIng");
	mask.setTransform(349,-3.5);

	// rocket
	this.rocketMC = new lib.rocketMC();
	this.rocketMC.setTransform(1.8,229.5,0.026,0.026,-15,0,0,-46.3,3.4);

	this.rocketMC.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.rocketMC).wait(1).to({regX:-45.4,regY:2.9,scaleX:0.48,scaleY:0.48,rotation:-17.2,x:301.2,y:182.4},56,cjs.Ease.get(-1)).to({regX:-45.6,regY:2,scaleX:1,scaleY:1,rotation:-41,x:654.6,y:-71.2},84,cjs.Ease.get(1)).to({regX:-45.5,rotation:-36.4,x:645.9,y:-33.5},84).to({regX:-45.6,rotation:-41,x:654.6,y:-71.2},84).to({rotation:-35,x:633.7,y:-22.3},10).to({regY:1.9,rotation:-1,x:900.2,y:-44.3},18,cjs.Ease.get(-1)).to({_off:true},1).wait(26));

	// graph
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF5B41").s().p("AgDAKIAAgSIAIAAIAAASg");
	this.shape.setTransform(-0.6,231);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF5B41").s().p("AgEAKIAAgSIAJAAIAAASg");
	this.shape_1.setTransform(-0.5,231);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF5B41").s().p("AgFAKIAAgSIALgBIAAATg");
	this.shape_2.setTransform(-0.4,231);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF5B41").s().p("AgIAKIAAgSIAQgBIAAATg");
	this.shape_3.setTransform(-0.2,230.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF5B41").s().p("AgLALIAAgSIAWgDQABAKAAALg");
	this.shape_4.setTransform(0.1,230.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF5B41").s().p("AgPALIAAgSQASgBAMgCIABAVg");
	this.shape_5.setTransform(0.5,230.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF5B41").s().p("AgUAMIAAgSQAYgBAQgEQACALgCAMg");
	this.shape_6.setTransform(1,230.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF5B41").s().p("AgaANIAAgSQAggCAUgFQACAMgCANg");
	this.shape_7.setTransform(1.6,230.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF5B41").s().p("AghAOIAAgSQApgDAYgGQADANgDAOg");
	this.shape_8.setTransform(2.3,230.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF5B41").s().p("AgoAPIAAgSQAygDAdgIQAEAOgDAPg");
	this.shape_9.setTransform(3.1,230.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF5B41").s().p("AgxARIAAgSQA9gFAjgKQAFARgEAQg");
	this.shape_10.setTransform(4,230.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF5B41").s().p("Ag6ASIAAgSQBJgFAqgMQAFASgFARg");
	this.shape_11.setTransform(4.9,230.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF5B41").s().p("AhFAUIAAgUQBXgEAwgPQAHAUgGATg");
	this.shape_12.setTransform(6,229.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF5B41").s().p("AhQAWIAAgUQBlgGA4gRQAHAWgGAVg");
	this.shape_13.setTransform(7.1,229.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF5B41").s().p("AhdAYIAAgVQB1gGBBgUQAIAYgHAXg");
	this.shape_14.setTransform(8.4,229.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF5B41").s().p("AhpAaIAAgVQCFgHBJgXQAKAagJAZg");
	this.shape_15.setTransform(9.7,229.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF5B41").s().p("Ah3AcIAAgUQCXgJBTgaQALAdgKAag");
	this.shape_16.setTransform(11.1,229.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF5B41").s().p("AiHAfIAAgVQCrgLBcgcQANAfgLAdg");
	this.shape_17.setTransform(12.6,228.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF5B41").s().p("AiWAhIAAgUQC/gNBnggQAOAigMAfg");
	this.shape_18.setTransform(14.2,228.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FF5B41").s().p("AinAkIAAgVQDUgPBygjQAQAmgOAhg");
	this.shape_19.setTransform(15.9,228.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FF5B41").s().p("Ai5AmIAAgTQDrgSB+gnQASAqgPAig");
	this.shape_20.setTransform(17.7,228);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF5B41").s().p("AjLApIAAgUQEDgTCKgqQATAsgQAlg");
	this.shape_21.setTransform(19.5,227.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FF5B41").s().p("AjfAtIAAgVQEdgVCXgvQAVAwgTApg");
	this.shape_22.setTransform(21.5,227.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FF5B41").s().p("AjzAwIAAgVQE2gXClgzQAXAzgUAsg");
	this.shape_23.setTransform(23.6,227);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FF5B41").s().p("AkJAzIAAgUQFSgZCzg4QAZA3gWAug");
	this.shape_24.setTransform(25.7,226.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF5B41").s().p("AkfA3IAAgUQFugcDCg9QAcA8gYAxg");
	this.shape_25.setTransform(27.9,226.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FF5B41").s().p("Ak2A7IAAgVQGMgeDRhCQAeBAgaA1g");
	this.shape_26.setTransform(30.3,225.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FF5B41").s().p("AlOA+IAAgUQGrggDhhHQAgBEgcA3g");
	this.shape_27.setTransform(32.7,225.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FF5B41").s().p("AlnBCIAAgUQHKgiDyhOQAjBKgeA6g");
	this.shape_28.setTransform(35.2,225.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FF5B41").s().p("AmBBHIAAgVQHsglEDhTQAlBOghA/g");
	this.shape_29.setTransform(37.8,224.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FF5B41").s().p("AmbBLIAAgVQIOgnEVhZQAnBSgjBDg");
	this.shape_30.setTransform(40.5,224.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FF5B41").s().p("Am3BPIAAgVQIxgqEoheQAqBXglBGg");
	this.shape_31.setTransform(43.3,223.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FF5B41").s().p("AnUBUIAAgVQJWgtE6hlQAtBdgnBKg");
	this.shape_32.setTransform(46.2,223.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FF5B41").s().p("AnxBZIAAgVQJ8gxFOhrQAvBjgqBOg");
	this.shape_33.setTransform(49.1,222.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FF5B41").s().p("AoPBeIAAgWQKigyFjhzQAyBogsBTg");
	this.shape_34.setTransform(52.2,222.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FF5B41").s().p("AouBiIAAgUQLKg3F3h5QA2BugvBWg");
	this.shape_35.setTransform(55.3,221.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FF5B41").s().p("ApPBoIAAgWQL0g4GNiBQA4BzgxBcg");
	this.shape_36.setTransform(58.6,221.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FF5B41").s().p("ApwBtIAAgVQMeg8GjiIQA8B5g1Bgg");
	this.shape_37.setTransform(61.9,220.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FF5B41").s().p("AqSBzIAAgWQNKhAG5iPQA/CAg3Blg");
	this.shape_38.setTransform(65.3,220.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FF5B41").s().p("Aq1B4IAAgWQN3hCHQiXQBDCGg7Bpg");
	this.shape_39.setTransform(68.9,219.5);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FF5B41").s().p("ArZB+IAAgWQOlhGHoifQBGCMg9Bvg");
	this.shape_40.setTransform(72.5,218.9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FF5B41").s().p("Ar9CEIAAgWQPThKIBinQBJCThAB0g");
	this.shape_41.setTransform(76.2,218.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FF5B41").s().p("AsjCKIAAgWQQEhNIaiwQBNCahEB5g");
	this.shape_42.setTransform(80,217.6);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FF5B41").s().p("AtJCQIAAgWQQ1hRIzi4QBRChhHB+g");
	this.shape_43.setTransform(83.8,217);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FF5B41").s().p("AtxCWIAAgWQRohVJNjAQBVCohKCDg");
	this.shape_44.setTransform(87.8,216.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FF5B41").s().p("AuZCdIAAgXQSbhYJpjJQBYCuhNCKg");
	this.shape_45.setTransform(91.9,215.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FF5B41").s().p("AvCCjIAAgWQTQhdKEjSQBcC2hQCPg");
	this.shape_46.setTransform(96,215);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FF5B41").s().p("AvsCqIAAgWQUGhhKgjcQBhC+hVCVg");
	this.shape_47.setTransform(100.2,214.3);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FF5B41").s().p("AwYCxIAAgWQU+hlK9jmQBkDHhYCag");
	this.shape_48.setTransform(104.6,213.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FF5B41").s().p("AxDC4IAAgXQV1hoLbjwQBpDPhcCgg");
	this.shape_49.setTransform(109,212.8);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FF5B41").s().p("AxwC/IAAgWQWvhuL4j5QBtDXhgCmg");
	this.shape_50.setTransform(113.6,212.1);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FF5B41").s().p("AyeDGIAAgWQXqhyMXkDQBxDehjCtg");
	this.shape_51.setTransform(118.2,211.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FF5B41").s().p("AzNDOIAAgXQYmh2M2kOQB2DohoCzg");
	this.shape_52.setTransform(122.9,210.5);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FF5B41").s().p("Az8DWIAAgXQZih7NWkZQB7DwhsC7g");
	this.shape_53.setTransform(127.7,209.7);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FF5B41").s().p("A0tDdIAAgWQahiAN2kjQB/D5hvDAg");
	this.shape_54.setTransform(132.6,208.9);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FF5B41").s().p("A1eDlIAAgXQbgiEOXkuQCEECh0DHg");
	this.shape_55.setTransform(137.5,208.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FF5B41").s().p("A2QDtIAAgXQcgiJO5k5QCIELh3DOg");
	this.shape_56.setTransform(142.6,207.2);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FF5B41").s().p("A20EEIAAgXQc/iHPjlpQCGEhh2Dmg");
	this.shape_57.setTransform(146.2,205);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FF5B41").s().p("A3XEbIAAgYQddiFQNmXQCCE2hyD+g");
	this.shape_58.setTransform(149.7,202.7);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FF5B41").s().p("A36ExIAAgXQd7iEQ2nGQCAFNhwEUg");
	this.shape_59.setTransform(153.2,200.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FF5B41").s().p("A4dFHIAAgYQeZiARfn1QB8FihsErg");
	this.shape_60.setTransform(156.6,198.3);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FF5B41").s().p("A4+FdIAAgYQe1h/SIoiQB5F3hqFCg");
	this.shape_61.setTransform(160,196.1);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FF5B41").s().p("A5gFyIAAgYQfTh8SvpPQB2GLhnFYg");
	this.shape_62.setTransform(163.4,194);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FF5B41").s().p("A6BGHIAAgXQfvh8TXp6QBzGghlFtg");
	this.shape_63.setTransform(166.7,191.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FF5B41").s().p("A6iGcIAAgYUAgMgB5AT9gKmQBwG0hiGDg");
	this.shape_64.setTransform(169.9,189.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FF5B41").s().p("A7CGxIAAgYUAgngB3AUkgLSQBtHIhgGZg");
	this.shape_65.setTransform(173.2,187.7);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FF5B41").s().p("A7iHFIAAgYUAhDgB1AVJgL8QBrHcheGtg");
	this.shape_66.setTransform(176.3,185.7);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FF5B41").s().p("A8BHZIAAgYUAhdgBzAVvgMmQBoHvhbHCg");
	this.shape_67.setTransform(179.5,183.7);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FF5B41").s().p("A8gHtIAAgZUAh4gBwAWTgNQQBlIChYHXg");
	this.shape_68.setTransform(182.6,181.7);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FF5B41").s().p("A8/IAIAAgYUAiTgBvAW3gN4QBiIVhVHqg");
	this.shape_69.setTransform(185.6,179.8);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FF5B41").s().p("A9dITIAAgYUAitgBtAXbgOgQBfInhTH+g");
	this.shape_70.setTransform(188.6,177.8);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FF5B41").s().p("A96ImIAAgYUAjGgBrAX+gPIQBdI6hRIRg");
	this.shape_71.setTransform(191.6,175.9);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FF5B41").s().p("A+YI5IAAgYUAjfgBqAYigPvQBaJMhPIlg");
	this.shape_72.setTransform(194.5,174.1);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FF5B41").s().p("A+1JLIAAgYUAj4gBoAZEgQVQBXJdhMI4g");
	this.shape_73.setTransform(197.4,172.2);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FF5B41").s().p("A/RJeIAAgZUAkQgBmAZmgQ8QBUJwhKJLg");
	this.shape_74.setTransform(200.3,170.4);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FF5B41").s().p("A/tJvIAAgYUAkpgBlAaGgRgQBSKAhHJdg");
	this.shape_75.setTransform(203,168.6);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FF5B41").s().p("EggIAKBIAAgZUAlAgBiAangSGQBQKRhGJwg");
	this.shape_76.setTransform(205.8,166.9);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FF5B41").s().p("EggkAKSIAAgYUAlYgBhAbHgSqQBNKihDKBg");
	this.shape_77.setTransform(208.5,165.1);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FF5B41").s().p("Egg+AKjIAAgYUAlugBgAbogTNQBKKyhBKTg");
	this.shape_78.setTransform(211.2,163.4);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FF5B41").s().p("EghZAK0IAAgZUAmFgBdAcHgTxQBILDg/Kkg");
	this.shape_79.setTransform(213.8,161.8);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FF5B41").s().p("EghyALFIAAgZUAmbgBcAclgUUQBGLTg9K2g");
	this.shape_80.setTransform(216.4,160.1);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FF5B41").s().p("EgiMALVIAAgZUAmxgBbAdEgU1QBDLjg7LGg");
	this.shape_81.setTransform(218.9,158.5);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FF5B41").s().p("EgilALlIAAgZUAnHgBZAdhgVXQBBLyg5LXg");
	this.shape_82.setTransform(221.4,156.9);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FF5B41").s().p("Egi9AL1IAAgZUAnbgBYAd/gV4QA/MCg3Lng");
	this.shape_83.setTransform(223.9,155.3);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FF5B41").s().p("EgjVAMEIAAgZUAnwgBWAebgWYQA9MQg2L3g");
	this.shape_84.setTransform(226.3,153.8);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FF5B41").s().p("EgjtAMTIAAgZUAoFgBUAe3gW4QA6MfgzMGg");
	this.shape_85.setTransform(228.7,152.3);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FF5B41").s().p("EgkEAMiIAAgZUAoZgBUAfSgXWQA5MtgyMWg");
	this.shape_86.setTransform(231,150.8);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FF5B41").s().p("EgkbAMxIAAgZUAosgBTAfugX1QA3M8gwMlg");
	this.shape_87.setTransform(233.3,149.3);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FF5B41").s().p("EgkyAM/IAAgZUApAgBRAgJgYTQA0NJguM0g");
	this.shape_88.setTransform(235.5,147.9);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FF5B41").s().p("EglIANNIAAgZUApTgBQAgjgYwQAyNXgsNCg");
	this.shape_89.setTransform(237.7,146.5);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FF5B41").s().p("EgldANbIAAgaUAplgBNAg8gZNQAxNjgrNRg");
	this.shape_90.setTransform(239.9,145.1);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FF5B41").s().p("EglyANoIAAgaUAp3gBMAhWgZpQAuNxgpNeg");
	this.shape_91.setTransform(242,143.8);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FF5B41").s().p("EgmHAN1IAAgZUAqJgBLAhugaFQAtN+gnNrg");
	this.shape_92.setTransform(244.1,142.4);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FF5B41").s().p("EgmbAOCIAAgZUAqbgBKAiGgagQArOKgmN5g");
	this.shape_93.setTransform(246.1,141.1);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FF5B41").s().p("EgmvAOPIAAgaUAqsgBIAidga7QApOXgkOGg");
	this.shape_94.setTransform(248.1,139.9);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FF5B41").s().p("EgnDAObIAAgZUAq9gBIAi1gbUQAnOigjOTg");
	this.shape_95.setTransform(250,138.6);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FF5B41").s().p("EgnWAOoIAAgaUArNgBHAjMgbtQAlOughOgg");
	this.shape_96.setTransform(251.9,137.4);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FF5B41").s().p("EgnoAOzIAAgZUArdgBGAjhgcGQAkO6gfOrg");
	this.shape_97.setTransform(253.8,136.2);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FF5B41").s().p("Egn6AO/IAAgaUArsgBDAj3gcgQAjPFgeO4g");
	this.shape_98.setTransform(255.6,135.1);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FF5B41").s().p("EgoMAPKIAAgZUAr7gBDAkNgc3QAgPQgcPDg");
	this.shape_99.setTransform(257.4,133.9);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FF5B41").s().p("EgodAPVIAAgZUAsKgBCAkhgdOQAfPbgbPOg");
	this.shape_100.setTransform(259.1,132.8);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FF5B41").s().p("EgouAPgIAAgaUAsZgBAAk1gdlQAdPlgaPag");
	this.shape_101.setTransform(260.8,131.8);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#FF5B41").s().p("Ego/APrIAAgaUAsogBAAlIgd6QAcPvgZPlg");
	this.shape_102.setTransform(262.4,130.7);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FF5B41").s().p("EgpPAP1IAAgaUAs1gA/AlcgeQQAaP6gXPvg");
	this.shape_103.setTransform(264,129.7);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#FF5B41").s().p("EgpeAP/IAAgaUAtCgA+AlugelQAZQDgWP6g");
	this.shape_104.setTransform(265.6,128.7);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#FF5B41").s().p("EgptAQJIAAgaUAtPgA9AmAge6QAYQNgVQEg");
	this.shape_105.setTransform(267.1,127.7);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#FF5B41").s().p("Egp8AQSIAAgaUAtcgA8AmRgfNQAXQWgUQNg");
	this.shape_106.setTransform(268.6,126.8);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#FF5B41").s().p("EgqLAQbIAAgaUAtpgA7AmigfgQAVQegSQXg");
	this.shape_107.setTransform(270,125.9);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#FF5B41").s().p("EgqYAQkIAAgaUAt0gA7AmzgfyQAUQngSQgg");
	this.shape_108.setTransform(271.4,125);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#FF5B41").s().p("EgqmAQtIAAgaUAuAgA6AnDggFQATQwgRQpg");
	this.shape_109.setTransform(272.7,124.1);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#FF5B41").s().p("EgqzAQ1IAAgaUAuLgA5AnTggWQARQ3gPQyg");
	this.shape_110.setTransform(274,123.3);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#FF5B41").s().p("Egq/AQ9IAAgaUAuWgA4AnhggnQAQQ/gOQ6g");
	this.shape_111.setTransform(275.3,122.5);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#FF5B41").s().p("EgrMARFIAAgaUAuhgA4Anvgg2QAQRGgORCg");
	this.shape_112.setTransform(276.5,121.7);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#FF5B41").s().p("EgrXARMIAAgaUAuqgA2An+ghHQAORNgMRKg");
	this.shape_113.setTransform(277.7,121);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#FF5B41").s().p("EgrjARTIAAgaUAu1gA2AoLghVQANRUgMRRg");
	this.shape_114.setTransform(278.8,120.2);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#FF5B41").s().p("EgruARaIAAgaUAu+gA1AoYghkQAMRbgLRYg");
	this.shape_115.setTransform(279.9,119.5);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#FF5B41").s().p("Egr4ARhIAAgaUAvGgA1AolghyQALRigKRfg");
	this.shape_116.setTransform(281,118.9);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#FF5B41").s().p("EgsCARnIAAgaUAvPgA0Aowgh/QALRogJRlg");
	this.shape_117.setTransform(282,118.2);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#FF5B41").s().p("EgsMARuIAAgbUAvYgAzAo8giMQAJRugIRsg");
	this.shape_118.setTransform(282.9,117.6);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#FF5B41").s().p("EgsVARzIAAgaUAvggAzApGgiYQAJRzgIRyg");
	this.shape_119.setTransform(283.8,117);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#FF5B41").s().p("EgseAR5IAAgbUAvngAxApRgilQAIR5gHR4g");
	this.shape_120.setTransform(284.7,116.5);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#FF5B41").s().p("EgsmAR+IAAgaUAvugAyApbgivQAHR+gGR9g");
	this.shape_121.setTransform(285.5,115.9);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#FF5B41").s().p("EgsuASDIAAgaUAv1gAxAplgi6QAGSDgGSCg");
	this.shape_122.setTransform(286.3,115.4);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#FF5B41").s().p("Egs1ASIIAAgbUAv7gAwAptgjEQAGSIgFSHg");
	this.shape_123.setTransform(287.1,115);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#FF5B41").s().p("Egs8ASNIAAgbUAwBgAwAp2gjNQAFSLgFSNg");
	this.shape_124.setTransform(287.8,114.5);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#FF5B41").s().p("EgtDASRIAAgbUAwHgAwAp+gjWQAESQgESRg");
	this.shape_125.setTransform(288.4,114.1);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#FF5B41").s().p("EgtJASVIAAgbUAwMgAvAqFgjfQAESUgESVg");
	this.shape_126.setTransform(289.1,113.7);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#FF5B41").s().p("EgtPASZIAAgbUAwSgAvAqLgjnQAESYgDSZg");
	this.shape_127.setTransform(289.6,113.3);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#FF5B41").s().p("EgtUAScIAAgbUAwWgAvAqSgjtQADSbgDScg");
	this.shape_128.setTransform(290.2,113);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#FF5B41").s().p("EgtZASfIAAgbUAwagAuAqYgj0QACSegCSfg");
	this.shape_129.setTransform(290.7,112.7);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#FF5B41").s().p("EgteASiIAAgbUAwegAuAqegj6QABShgBSig");
	this.shape_130.setTransform(291.1,112.4);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#FF5B41").s().p("EgtiASlIAAgbUAwigAuAqigkAQABSkgBSlg");
	this.shape_131.setTransform(291.5,112.1);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#FF5B41").s().p("EgtlASnIAAgbUAwkgAuAqngkEQABSmgBSng");
	this.shape_132.setTransform(291.9,111.9);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#FF5B41").s().p("EgtpASpIAAgbUAwogAtAqqgkJQABSogBSpg");
	this.shape_133.setTransform(292.2,111.7);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#FF5B41").s().p("EgtrASrIAAgbUAwqgAtAqtgkNMAAAAlVg");
	this.shape_134.setTransform(292.5,111.5);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#FF5B41").s().p("EgtuASsIAAgbUAwsgAtAqwgkPMAAAAlXg");
	this.shape_135.setTransform(292.7,111.4);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#FF5B41").s().p("EgtwAStIAAgaUAwugAtAqygkSMAAAAlZg");
	this.shape_136.setTransform(292.9,111.2);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#FF5B41").s().p("EgtxASuIAAgaUAwvgAtAq0gkUMAAAAlbg");
	this.shape_137.setTransform(293.1,111.1);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#FF5B41").s().p("EgtyASvIAAgbUAwwgAsAq1gkWMAAAAldg");
	this.shape_138.setTransform(293.2,111.1);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#FF5B41").s().p("EgtzASvIAAgaUAwxgAtAq2gkWMAAAAldg");
	this.shape_139.setTransform(293.2,111);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#FF5B41").s().p("EgtzASwIAAgbUAwxgAtAq2gkXMAAAAlfg");
	this.shape_140.setTransform(293.3,111);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#FF5B41").s().p("EgtyAStIAAgaUAwtgAvAq4gkQMAAAAlZg");
	this.shape_141.setTransform(293.2,111.2);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#FF5B41").s().p("EgtxASrIAAgaUAwrgAwAq4gkLMAAAAlVg");
	this.shape_142.setTransform(293.1,111.4);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#FF5B41").s().p("EgtwASpIAAgbUAwogAxAq5gkFMAAAAlRg");
	this.shape_143.setTransform(293,111.7);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#FF5B41").s().p("EgtvASnIAAgbUAwlgAyAq6gkAMAAAAlNg");
	this.shape_144.setTransform(292.9,111.9);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#FF5B41").s().p("EgtuASlIAAgbUAwigA0Aq7gj6MAAAAlJg");
	this.shape_145.setTransform(292.8,112.1);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#FF5B41").s().p("EgttASjIAAgbUAwggA1Aq7gj1MAAAAlFg");
	this.shape_146.setTransform(292.7,112.3);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#FF5B41").s().p("EgtsAShIAAgbUAwdgA3Aq8gjvMAAAAlBg");
	this.shape_147.setTransform(292.6,112.5);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#FF5B41").s().p("EgtrASeIAAgaUAwagA5Aq9gjoMAAAAk7g");
	this.shape_148.setTransform(292.5,112.7);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#FF5B41").s().p("EgtqAScIAAgaUAwXgA6Aq+gjjMAAAAk3g");
	this.shape_149.setTransform(292.4,112.9);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#FF5B41").s().p("EgtpASaIAAgbUAwVgA6Aq+gjeMAAAAkzg");
	this.shape_150.setTransform(292.3,113.2);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#FF5B41").s().p("EgtoASYIAAgbUAwSgA8Aq/gjYMAAAAkvg");
	this.shape_151.setTransform(292.2,113.4);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#FF5B41").s().p("EgtnASWIAAgbUAwPgA+ArAgjSMAAAAkrg");
	this.shape_152.setTransform(292.1,113.6);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#FF5B41").s().p("EgtmASUIAAgbUAwNgA/ArAgjNMAAAAkng");
	this.shape_153.setTransform(292,113.8);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#FF5B41").s().p("EgtlASSIAAgbUAwJgBBArCgjGMAAAAkig");
	this.shape_154.setTransform(291.9,114);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#FF5B41").s().p("EgtkASPIAAgaUAwHgBDArCgjAMAAAAkdg");
	this.shape_155.setTransform(291.8,114.2);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#FF5B41").s().p("EgtjASNIAAgaUAwEgBEArDgi7MAAAAkZg");
	this.shape_156.setTransform(291.7,114.4);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#FF5B41").s().p("EgtiASLIAAgbUAwCgBFArDgi1MAAAAkVg");
	this.shape_157.setTransform(291.6,114.7);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#FF5B41").s().p("EgthASJIAAgbUAv+gBGArFgiwMAAAAkRg");
	this.shape_158.setTransform(291.5,114.9);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#FF5B41").s().p("EgtgASHIAAgbUAv8gBIArFgiqMAAAAkNg");
	this.shape_159.setTransform(291.4,115.1);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#FF5B41").s().p("EgtfASFIAAgbUAv5gBJArGgilMAAAAkJg");
	this.shape_160.setTransform(291.3,115.3);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#FF5B41").s().p("EgteASDIAAgbUAv3gBKArGgigMAAAAkFg");
	this.shape_161.setTransform(291.2,115.5);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#FF5B41").s().p("EgtdASAIAAgaUAvzgBMArIgiZMAAAAj/g");
	this.shape_162.setTransform(291.1,115.7);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#FF5B41").s().p("EgtcAR+IAAgaUAvxgBOArIgiTMAAAAj7g");
	this.shape_163.setTransform(291,115.9);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#FF5B41").s().p("EgtbAR8IAAgbUAvugBOArJgiOMAAAAj3g");
	this.shape_164.setTransform(290.9,116.2);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#FF5B41").s().p("EgtaAR6IAAgbUAvsgBQArJgiIMAAAAjzg");
	this.shape_165.setTransform(290.8,116.4);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("#FF5B41").s().p("EgtZAR4IAAgbUAvogBSArLgiCMAAAAjvg");
	this.shape_166.setTransform(290.7,116.6);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("#FF5B41").s().p("EgtYAR2IAAgbUAvmgBTArLgh9MAAAAjrg");
	this.shape_167.setTransform(290.6,116.8);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("#FF5B41").s().p("EgtXAR0IAAgbUAvjgBUArMgh4MAAAAjng");
	this.shape_168.setTransform(290.5,117);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("#FF5B41").s().p("EgtWARxIAAgaUAvggBWArNghxMAAAAjhg");
	this.shape_169.setTransform(290.4,117.2);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("#FF5B41").s().p("EgtVARvIAAgaUAvdgBYArOghrMAAAAjdg");
	this.shape_170.setTransform(290.3,117.4);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("#FF5B41").s().p("EgtUARtIAAgbUAvbgBYArOghmMAAAAjZg");
	this.shape_171.setTransform(290.2,117.7);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("#FF5B41").s().p("EgtTARrIAAgbUAvYgBaArPghgMAAAAjVg");
	this.shape_172.setTransform(290.1,117.9);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f("#FF5B41").s().p("EgtSARpIAAgbUAvVgBbArQghbMAAAAjRg");
	this.shape_173.setTransform(290,118.1);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f("#FF5B41").s().p("EgtRARnIAAgbUAvSgBdArRghVMAAAAjNg");
	this.shape_174.setTransform(289.9,118.3);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("#FF5B41").s().p("EgtQARlIAAgbUAvQgBfArRghOMAAAAjIg");
	this.shape_175.setTransform(289.8,118.5);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f("#FF5B41").s().p("EgtPARiIAAgaUAvNgBgArSghJMAAAAjDg");
	this.shape_176.setTransform(289.7,118.7);

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("#FF5B41").s().p("EgtOARgIAAgaUAvKgBiArTghDMAAAAi/g");
	this.shape_177.setTransform(289.6,118.9);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("#FF5B41").s().p("EgtNAReIAAgbUAvIgBiArTgg+MAAAAi7g");
	this.shape_178.setTransform(289.5,119.2);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#FF5B41").s().p("EgtMARcIAAgbUAvFgBjArUgg5MAAAAi3g");
	this.shape_179.setTransform(289.4,119.4);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("#FF5B41").s().p("EgtLARaIAAgbUAvCgBlArVggzMAAAAizg");
	this.shape_180.setTransform(289.3,119.6);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("#FF5B41").s().p("EgtKARYIAAgbUAu/gBnArWggtMAAAAivg");
	this.shape_181.setTransform(289.2,119.8);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("#FF5B41").s().p("EgtJARWIAAgbUAu9gBpArWggnMAAAAirg");
	this.shape_182.setTransform(289.1,120);

	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("#FF5B41").s().p("EgtIARTIAAgaUAu6gBqArXgghMAAAAilg");
	this.shape_183.setTransform(289,120.2);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("#FF5B41").s().p("EgtHARRIAAgaUAu2gBsArZggbMAAAAihg");
	this.shape_184.setTransform(288.9,120.4);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("#FF5B41").s().p("EgtGARPIAAgbUAu0gBsArZggWMAAAAidg");
	this.shape_185.setTransform(288.8,120.7);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("#FF5B41").s().p("EgtFARNIAAgbUAuxgBuAraggQMAAAAiZg");
	this.shape_186.setTransform(288.7,120.9);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("#FF5B41").s().p("EgtEARLIAAgbUAuugBvArbggLMAAAAiVg");
	this.shape_187.setTransform(288.6,121.1);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("#FF5B41").s().p("EgtDARJIAAgbUAurgBxArcggFMAAAAiRg");
	this.shape_188.setTransform(288.5,121.3);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("#FF5B41").s().p("EgtCARHIAAgbUAupgByArcggAMAAAAiNg");
	this.shape_189.setTransform(288.4,121.5);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("#FF5B41").s().p("EgtBAREIAAgaUAumgB0Ardgf5MAAAAiHg");
	this.shape_190.setTransform(288.3,121.7);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("#FF5B41").s().p("EgtAARCIAAgaUAujgB2AregfzMAAAAiDg");
	this.shape_191.setTransform(288.2,121.9);

	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("#FF5B41").s().p("Egs/ARAIAAgbUAuhgB2AregfuMAAAAh/g");
	this.shape_192.setTransform(288.1,122.2);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("#FF5B41").s().p("Egs+AQ+IAAgbUAuegB4ArfgfoMAAAAh7g");
	this.shape_193.setTransform(288,122.4);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("#FF5B41").s().p("Egs9AQ8IAAgbUAubgB5ArggfjMAAAAh3g");
	this.shape_194.setTransform(287.9,122.6);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("#FF5B41").s().p("Egs8AQ6IAAgbUAuYgB7ArhgfdMAAAAhzg");
	this.shape_195.setTransform(287.8,122.8);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("#FF5B41").s().p("Egs7AQ4IAAgbUAuWgB9ArhgfWMAAAAhug");
	this.shape_196.setTransform(287.7,123);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("#FF5B41").s().p("Egs6AQ1IAAgaUAuTgB+ArigfRMAAAAhpg");
	this.shape_197.setTransform(287.6,123.2);

	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("#FF5B41").s().p("Egs5AQzIAAgaUAuQgB/ArjgfMMAAAAhlg");
	this.shape_198.setTransform(287.5,123.4);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("#FF5B41").s().p("Egs4AQxIAAgbUAuNgCAArkgfGMAAAAhhg");
	this.shape_199.setTransform(287.4,123.7);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("#FF5B41").s().p("Egs3AQvIAAgbUAuLgCBArkgfBMAAAAhdg");
	this.shape_200.setTransform(287.3,123.9);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("#FF5B41").s().p("Egs2AQtIAAgbUAuHgCDArmge7MAAAAhZg");
	this.shape_201.setTransform(287.2,124.1);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("#FF5B41").s().p("Egs1AQrIAAgbUAuFgCFArmge1MAAAAhVg");
	this.shape_202.setTransform(287.1,124.3);

	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f("#FF5B41").s().p("Egs0AQpIAAgbUAuCgCGArngewMAAAAhRg");
	this.shape_203.setTransform(287,124.5);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("#FF5B41").s().p("EgszAQmIAAgaUAuAgCIArngepMAAAAhLg");
	this.shape_204.setTransform(286.9,124.7);

	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f("#FF5B41").s().p("EgsyAQkIAAgaUAt8gCJArpgekMAAAAhHg");
	this.shape_205.setTransform(286.8,124.9);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("#FF5B41").s().p("EgsxAQiIAAgbUAt6gCKArpgeeMAAAAhDg");
	this.shape_206.setTransform(286.7,125.2);

	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f("#FF5B41").s().p("EgswAQgIAAgbUAt3gCLArqgeZMAAAAg/g");
	this.shape_207.setTransform(286.6,125.4);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("#FF5B41").s().p("EgsvAQeIAAgbUAt0gCNArrgeTMAAAAg7g");
	this.shape_208.setTransform(286.5,125.6);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("#FF5B41").s().p("EgsuAQcIAAgbUAtxgCOArsgeOMAAAAg3g");
	this.shape_209.setTransform(286.4,125.8);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("#FF5B41").s().p("EgstAQaIAAgbUAtvgCQArsgeIMAAAAgzg");
	this.shape_210.setTransform(286.3,126);

	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f("#FF5B41").s().p("EgssAQXIAAgaUAtsgCSArtgeBMAAAAgtg");
	this.shape_211.setTransform(286.2,126.2);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("#FF5B41").s().p("EgsrAQVIAAgaUAtpgCTArugd8MAAAAgpg");
	this.shape_212.setTransform(286.1,126.4);

	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("#FF5B41").s().p("EgsqAQTIAAgbUAtmgCUArvgd2MAAAAglg");
	this.shape_213.setTransform(286,126.7);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("#FF5B41").s().p("EgspAQRIAAgbUAtkgCWArvgdwMAAAAghg");
	this.shape_214.setTransform(285.9,126.9);

	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f("#FF5B41").s().p("EgsoAQPIAAgbUAthgCXArwgdrMAAAAgdg");
	this.shape_215.setTransform(285.8,127.1);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("#FF5B41").s().p("EgsnAQNIAAgbUAtegCYArxgdmMAAAAgZg");
	this.shape_216.setTransform(285.7,127.3);

	this.shape_217 = new cjs.Shape();
	this.shape_217.graphics.f("#FF5B41").s().p("EgsmAQLIAAgbUAtcgCaArxgdfMAAAAgUg");
	this.shape_217.setTransform(285.6,127.5);

	this.shape_218 = new cjs.Shape();
	this.shape_218.graphics.f("#FF5B41").s().p("EgslAQIIAAgaUAtZgCcArygdZMAAAAgPg");
	this.shape_218.setTransform(285.5,127.7);

	this.shape_219 = new cjs.Shape();
	this.shape_219.graphics.f("#FF5B41").s().p("EgskAQGIAAgaUAtWgCdArzgdUMAAAAgLg");
	this.shape_219.setTransform(285.4,127.9);

	this.shape_220 = new cjs.Shape();
	this.shape_220.graphics.f("#FF5B41").s().p("EgsjAQEIAAgbUAtTgCeAr0gdOMAAAAgHg");
	this.shape_220.setTransform(285.3,128.2);

	this.shape_221 = new cjs.Shape();
	this.shape_221.graphics.f("#FF5B41").s().p("EgsiAQCIAAgbUAtRgCfAr0gdJMAAAAgDg");
	this.shape_221.setTransform(285.2,128.4);

	this.shape_222 = new cjs.Shape();
	this.shape_222.graphics.f("#FF5B41").s().p("EgshAQAIAAgbUAtOgChAr1gdDIAAf/g");
	this.shape_222.setTransform(285.1,128.6);

	this.shape_223 = new cjs.Shape();
	this.shape_223.graphics.f("#FF5B41").s().p("EgsgAP+IAAgbUAtLgCiAr2gc+IAAf7g");
	this.shape_223.setTransform(285,128.8);

	this.shape_224 = new cjs.Shape();
	this.shape_224.graphics.f("#FF5B41").s().p("EgsfAP8IAAgbUAtIgCkAr3gc4IAAf3g");
	this.shape_224.setTransform(284.9,129);

	this.shape_225 = new cjs.Shape();
	this.shape_225.graphics.f("#FF5B41").s().p("EgtrASeIAAgbUAwagA5Aq9gjnQhPTTBPRog");
	this.shape_225.setTransform(292.4,112.8);

	this.shape_226 = new cjs.Shape();
	this.shape_226.graphics.f("#FF5B41").s().p("EgtiASMIAAgbUAwCgBFArDgi3QicT4CcQfg");
	this.shape_226.setTransform(291.6,114.6);

	this.shape_227 = new cjs.Shape();
	this.shape_227.graphics.f("#FF5B41").s().p("EgtaAR6IAAgbUAvrgBRArKgiHQjrUcDrPXg");
	this.shape_227.setTransform(290.7,116.4);

	this.shape_228 = new cjs.Shape();
	this.shape_228.graphics.f("#FF5B41").s().p("EgtSARoIAAgbUAvUgBdArQghWQk4U/E4OPg");
	this.shape_228.setTransform(289.9,118.2);

	this.shape_229 = new cjs.Shape();
	this.shape_229.graphics.f("#FF5B41").s().p("EgtJARWIAAgbUAu9gBpArWggnQmHVlGHNGg");
	this.shape_229.setTransform(289.1,120);

	this.shape_230 = new cjs.Shape();
	this.shape_230.graphics.f("#FF5B41").s().p("EgtBAREIAAgbUAulgB0Ardgf4QnUWJHUL+g");
	this.shape_230.setTransform(288.2,121.8);

	this.shape_231 = new cjs.Shape();
	this.shape_231.graphics.f("#FF5B41").s().p("Egs4AQyIAAgbUAuOgCAArjgfIQojWuIjK1g");
	this.shape_231.setTransform(287.4,123.6);

	this.shape_232 = new cjs.Shape();
	this.shape_232.graphics.f("#FF5B41").s().p("EgswAQgIAAgbUAt3gCMArqgeYQpxXTJxJsg");
	this.shape_232.setTransform(286.5,125.4);

	this.shape_233 = new cjs.Shape();
	this.shape_233.graphics.f("#FF5B41").s().p("EgsoAQOIAAgbUAtggCYArxgdoQrAX3LAIkg");
	this.shape_233.setTransform(285.7,127.2);

	this.shape_234 = new cjs.Shape();
	this.shape_234.graphics.f("#FF5B41").s().p("EgsfAP8IAAgbUAtIgCkAr3gc4QsNYbMNHcg");
	this.shape_234.setTransform(284.9,129);

	this.shape_235 = new cjs.Shape();
	this.shape_235.graphics.f("#FF5B41").s().p("EgsjAP8IAAgaUAtWgCvArxgcuQsHYeMGHZg");
	this.shape_235.setTransform(285.2,128.9);

	this.shape_236 = new cjs.Shape();
	this.shape_236.graphics.f("#FF5B41").s().p("EgstAP/IAAgbUAt+gDOArdgcUQrwYnLuHWg");
	this.shape_236.setTransform(286.3,128.7);

	this.shape_237 = new cjs.Shape();
	this.shape_237.graphics.f("#FF5B41").s().p("Egs/AQDIAAgbUAvDgEEAq8gbmQrLY3LFHOg");
	this.shape_237.setTransform(288.1,128.3);

	this.shape_238 = new cjs.Shape();
	this.shape_238.graphics.f("#FF5B41").s().p("EgtYAQJIAAgbUAwigFOAqPgaoQqXZNKNHEg");
	this.shape_238.setTransform(290.5,127.7);

	this.shape_239 = new cjs.Shape();
	this.shape_239.graphics.f("#FF5B41").s().p("Egt4AQQIAAgbUAycgGuApVgZWQpUZnJFG4g");
	this.shape_239.setTransform(293.7,127);

	this.shape_240 = new cjs.Shape();
	this.shape_240.graphics.f("#FF5B41").s().p("EgufAQZIAAgbUA0xgIjAoOgXzQoCaKHtGng");
	this.shape_240.setTransform(297.6,126.1);

	this.shape_241 = new cjs.Shape();
	this.shape_241.graphics.f("#FF5B41").s().p("EgvNAQkIAAgbUA3igKuAm5gV+QmiayGFGVg");
	this.shape_241.setTransform(302.2,125);

	this.shape_242 = new cjs.Shape();
	this.shape_242.graphics.f("#FF5B41").s().p("EgwCAQwIAAgbUA6tgNNAlYgT3QkybfENGAg");
	this.shape_242.setTransform(307.6,123.8);

	this.shape_243 = new cjs.Shape();
	this.shape_243.graphics.f("#FF5B41").s().p("Egw+AQ+IAAgbUA+UgQDAjpgRdQizcUCEFng");
	this.shape_243.setTransform(313.6,122.4);

	this.shape_244 = new cjs.Shape();
	this.shape_244.graphics.f("#FF5B41").s().p("EgyCAROIAAgbUBCXgTMAhugOzQgndNgTFNg");
	this.shape_244.setTransform(320.3,120.8);

	this.shape_245 = new cjs.Shape();
	this.shape_245.graphics.f("#FF5B41").s().p("EgzjARfIAAgbUBG0gWrAflgL3QB2ePi8Eug");
	this.shape_245.setTransform(330,119.1);

	this.shape_246 = new cjs.Shape();
	this.shape_246.graphics.f("#FF5B41").s().p("Eg1dARyIAAgbUBLtgagAdQgInQEgfUl0EOg");
	this.shape_246.setTransform(342.2,117.2);

	this.shape_247 = new cjs.Shape();
	this.shape_247.graphics.f("#FF5B41").s().p("Eg3iASGIAAgbUBRAgeqAaugFGUAHbAgigI9ADpg");
	this.shape_247.setTransform(355.5,115.2);

	this.shape_248 = new cjs.Shape();
	this.shape_248.graphics.f("#FF5B41").s().p("Eg5yAScIAAgaUBWvgjLAX+gBSUAKkAh0gMVADDg");
	this.shape_248.setTransform(369.9,112.9);

	this.shape_249 = new cjs.Shape();
	this.shape_249.graphics.f("#FF5B41").s().p("Eg8MAS6IAAgbUBc5goAAVCAC0UAN6AjNgP9ACag");
	this.shape_249.setTransform(385.4,110);

	this.shape_250 = new cjs.Shape();
	this.shape_250.graphics.f("#FF5B41").s().p("Eg+yATtIAAgbUBjegtKAR5AHLUARhAktgT1ABtg");
	this.shape_250.setTransform(401.9,104.9);

	this.shape_251 = new cjs.Shape();
	this.shape_251.graphics.f("#FF5B41").s().p("EhBiAUwIAAgaUBqfgyqAOiAL0UAVWAmSgX9AA+g");
	this.shape_251.setTransform(419.5,98.1);

	this.shape_252 = new cjs.Shape();
	this.shape_252.graphics.f("#FF5B41").s().p("EhEcAWAIAAgbUBx6g4fAK/AQwUAZaAn+gcWAAMg");
	this.shape_252.setTransform(438.2,90.2);

	this.shape_253 = new cjs.Shape();
	this.shape_253.graphics.f("rgba(255,91,65,0.925)").s().p("EhEWAUkIAAgaUBwTg0wAMkAPnQIzLaCJINQEsR0zOAIg");
	this.shape_253.setTransform(437.6,99.3);

	this.shape_254 = new cjs.Shape();
	this.shape_254.graphics.f("rgba(255,91,65,0.851)").s().p("EhESATMIgCAAIAAgaUBuxgxLAOFAOhQJwKmBkH8QDPQbyyAHg");
	this.shape_254.setTransform(437.3,108.1);

	this.shape_255 = new cjs.Shape();
	this.shape_255.graphics.f("rgba(255,91,65,0.784)").s().p("EhETAR4IgCAAIAAgaUBtSgtuAPjANdQKrJ0BAHsQB3PFyYAGg");
	this.shape_255.setTransform(437.4,116.5);

	this.shape_256 = new cjs.Shape();
	this.shape_256.graphics.f("rgba(255,91,65,0.718)").s().p("EhEZAQnIgBAAIAAgaUBr3gqcAQ8AMdQLkJFAdHbQAiNzx/AGg");
	this.shape_256.setTransform(438,124.6);

	this.shape_257 = new cjs.Shape();
	this.shape_257.graphics.f("rgba(255,91,65,0.651)").s().p("EhEiAPaIgCAAIAAgaUBqggnSASSALfQMaIXgDHMQgwMlxmAFg");
	this.shape_257.setTransform(438.9,132.3);

	this.shape_258 = new cjs.Shape();
	this.shape_258.graphics.f("rgba(255,91,65,0.592)").s().p("EhEtAORIgBgBIAAgaUBpOgkRATjAKkQNNHrgiG+Qh9LaxQAFg");
	this.shape_258.setTransform(439.9,139.7);

	this.shape_259 = new cjs.Shape();
	this.shape_259.graphics.f("rgba(255,91,65,0.533)").s().p("EhE4ANLIgBgBIAAgaUBn/ghbAUxAJsQN+HDhAGwQjHKSw6AFg");
	this.shape_259.setTransform(441,146.7);

	this.shape_260 = new cjs.Shape();
	this.shape_260.graphics.f("rgba(255,91,65,0.478)").s().p("EhFCAMIIgBgBIAAgZUBm0geuAV6AI3QOtGbhcGjQkNJPwlAEg");
	this.shape_260.setTransform(442.1,153.3);

	this.shape_261 = new cjs.Shape();
	this.shape_261.graphics.f("rgba(255,91,65,0.427)").s().p("EhFNALKIgBgBIAAgaUBlugcJAXAAIFQPZF1h3GXQlPIPwSAEg");
	this.shape_261.setTransform(443.1,159.6);

	this.shape_262 = new cjs.Shape();
	this.shape_262.graphics.f("rgba(255,91,65,0.38)").s().p("EhFXAKOIgBgBIAAgZUBkrgZvAYCAHVQQCFTiQGLQmNHTv/ADg");
	this.shape_262.setTransform(444.1,165.5);

	this.shape_263 = new cjs.Shape();
	this.shape_263.graphics.f("rgba(255,91,65,0.333)").s().p("EhFhAJXIAAgCIAAgZUBjsgXdAY/AGpQQqExioGBQnIGavuADg");
	this.shape_263.setTransform(445.1,171.1);

	this.shape_264 = new cjs.Shape();
	this.shape_264.graphics.f("rgba(255,91,65,0.29)").s().p("EhFqAIjIgBgCIAAgZUBiygVVAZ5AF/QROETi+F2Qn+FlveADg");
	this.shape_264.setTransform(446,176.3);

	this.shape_265 = new cjs.Shape();
	this.shape_265.graphics.f("rgba(255,91,65,0.251)").s().p("EhFzAHyIgBgBIAAgZUBh8gTXAauAFYQRwD3jSFsQoyE0vPACg");
	this.shape_265.setTransform(446.9,181.1);

	this.shape_266 = new cjs.Shape();
	this.shape_266.graphics.f("rgba(255,91,65,0.212)").s().p("EhF7AHGIgBgCIAAgZUBhKgRiAbfAE1QSQDcjmFjQpgEHvBACg");
	this.shape_266.setTransform(447.7,185.6);

	this.shape_267 = new cjs.Shape();
	this.shape_267.graphics.f("rgba(255,91,65,0.18)").s().p("EhGDAGcIAAgBIAAgZUBgbgP2AcNAETQSsDEj3FbQqMDdu0ABg");
	this.shape_267.setTransform(448.5,189.7);

	this.shape_268 = new cjs.Shape();
	this.shape_268.graphics.f("rgba(255,91,65,0.149)").s().p("EhGKAF3IgBgCIAAgZUBfxgOTAc3AD1QTHCtkHFUQq0C2uoACg");
	this.shape_268.setTransform(449.2,193.5);

	this.shape_269 = new cjs.Shape();
	this.shape_269.graphics.f("rgba(255,91,65,0.122)").s().p("EhGRAFVIAAgbUBfLgM6AdcADaQTfCZkVFNQrYCUueABg");
	this.shape_269.setTransform(449.8,196.9);

	this.shape_270 = new cjs.Shape();
	this.shape_270.graphics.f("rgba(255,91,65,0.094)").s().p("EhGXAE2IAAgaUBeogLrAd/ADCQT0CHkjFHQr3B0uUABg");
	this.shape_270.setTransform(450.4,199.9);

	this.shape_271 = new cjs.Shape();
	this.shape_271.graphics.f("rgba(255,91,65,0.071)").s().p("EhGcAEbIAAgaUBeKgKlAecACsQUHB3kuFCQsUBauLAAg");
	this.shape_271.setTransform(450.9,202.6);

	this.shape_272 = new cjs.Shape();
	this.shape_272.graphics.f("rgba(255,91,65,0.055)").s().p("EhGgAEEIAAgaUBdvgJoAe2ACaQUYBpk4E9QstBCuEAAg");
	this.shape_272.setTransform(451.4,204.9);

	this.shape_273 = new cjs.Shape();
	this.shape_273.graphics.f("rgba(255,91,65,0.035)").s().p("EhGkADwIAAgaUBdZgI0AfMACJQUlBelAE5QtBAut/AAg");
	this.shape_273.setTransform(451.8,206.9);

	this.shape_274 = new cjs.Shape();
	this.shape_274.graphics.f("rgba(255,91,65,0.024)").s().p("EhGnADgIAAgaUBdHgIKAfeAB9QUwBUlHE2QtSAdt5AAg");
	this.shape_274.setTransform(452.1,208.5);

	this.shape_275 = new cjs.Shape();
	this.shape_275.graphics.f("rgba(255,91,65,0.012)").s().p("EhGqADUIAAgbUBc5gHpAfsABzQU5BNlMEzQtfARt2AAg");
	this.shape_275.setTransform(452.3,209.8);

	this.shape_276 = new cjs.Shape();
	this.shape_276.graphics.f("rgba(255,91,65,0.008)").s().p("EhGrADLIAAgbUBcvgHSAf1ABsQVABHlQEyQtpAItzAAg");
	this.shape_276.setTransform(452.5,210.7);

	this.shape_277 = new cjs.Shape();
	this.shape_277.graphics.f("rgba(255,91,65,0)").s().p("EhGsADGIAAgbUBcpgHDAf7ABnQVEBElTExQtuACtxAAg");
	this.shape_277.setTransform(452.6,211.2);

	this.shape_278 = new cjs.Shape();
	this.shape_278.graphics.f("rgba(255,91,65,0)").s().p("EhGtADEIAAgbUCZsgLngNWAMCg");
	this.shape_278.setTransform(452.6,211.4);

	this.shape.mask = this.shape_1.mask = this.shape_2.mask = this.shape_3.mask = this.shape_4.mask = this.shape_5.mask = this.shape_6.mask = this.shape_7.mask = this.shape_8.mask = this.shape_9.mask = this.shape_10.mask = this.shape_11.mask = this.shape_12.mask = this.shape_13.mask = this.shape_14.mask = this.shape_15.mask = this.shape_16.mask = this.shape_17.mask = this.shape_18.mask = this.shape_19.mask = this.shape_20.mask = this.shape_21.mask = this.shape_22.mask = this.shape_23.mask = this.shape_24.mask = this.shape_25.mask = this.shape_26.mask = this.shape_27.mask = this.shape_28.mask = this.shape_29.mask = this.shape_30.mask = this.shape_31.mask = this.shape_32.mask = this.shape_33.mask = this.shape_34.mask = this.shape_35.mask = this.shape_36.mask = this.shape_37.mask = this.shape_38.mask = this.shape_39.mask = this.shape_40.mask = this.shape_41.mask = this.shape_42.mask = this.shape_43.mask = this.shape_44.mask = this.shape_45.mask = this.shape_46.mask = this.shape_47.mask = this.shape_48.mask = this.shape_49.mask = this.shape_50.mask = this.shape_51.mask = this.shape_52.mask = this.shape_53.mask = this.shape_54.mask = this.shape_55.mask = this.shape_56.mask = this.shape_57.mask = this.shape_58.mask = this.shape_59.mask = this.shape_60.mask = this.shape_61.mask = this.shape_62.mask = this.shape_63.mask = this.shape_64.mask = this.shape_65.mask = this.shape_66.mask = this.shape_67.mask = this.shape_68.mask = this.shape_69.mask = this.shape_70.mask = this.shape_71.mask = this.shape_72.mask = this.shape_73.mask = this.shape_74.mask = this.shape_75.mask = this.shape_76.mask = this.shape_77.mask = this.shape_78.mask = this.shape_79.mask = this.shape_80.mask = this.shape_81.mask = this.shape_82.mask = this.shape_83.mask = this.shape_84.mask = this.shape_85.mask = this.shape_86.mask = this.shape_87.mask = this.shape_88.mask = this.shape_89.mask = this.shape_90.mask = this.shape_91.mask = this.shape_92.mask = this.shape_93.mask = this.shape_94.mask = this.shape_95.mask = this.shape_96.mask = this.shape_97.mask = this.shape_98.mask = this.shape_99.mask = this.shape_100.mask = this.shape_101.mask = this.shape_102.mask = this.shape_103.mask = this.shape_104.mask = this.shape_105.mask = this.shape_106.mask = this.shape_107.mask = this.shape_108.mask = this.shape_109.mask = this.shape_110.mask = this.shape_111.mask = this.shape_112.mask = this.shape_113.mask = this.shape_114.mask = this.shape_115.mask = this.shape_116.mask = this.shape_117.mask = this.shape_118.mask = this.shape_119.mask = this.shape_120.mask = this.shape_121.mask = this.shape_122.mask = this.shape_123.mask = this.shape_124.mask = this.shape_125.mask = this.shape_126.mask = this.shape_127.mask = this.shape_128.mask = this.shape_129.mask = this.shape_130.mask = this.shape_131.mask = this.shape_132.mask = this.shape_133.mask = this.shape_134.mask = this.shape_135.mask = this.shape_136.mask = this.shape_137.mask = this.shape_138.mask = this.shape_139.mask = this.shape_140.mask = this.shape_141.mask = this.shape_142.mask = this.shape_143.mask = this.shape_144.mask = this.shape_145.mask = this.shape_146.mask = this.shape_147.mask = this.shape_148.mask = this.shape_149.mask = this.shape_150.mask = this.shape_151.mask = this.shape_152.mask = this.shape_153.mask = this.shape_154.mask = this.shape_155.mask = this.shape_156.mask = this.shape_157.mask = this.shape_158.mask = this.shape_159.mask = this.shape_160.mask = this.shape_161.mask = this.shape_162.mask = this.shape_163.mask = this.shape_164.mask = this.shape_165.mask = this.shape_166.mask = this.shape_167.mask = this.shape_168.mask = this.shape_169.mask = this.shape_170.mask = this.shape_171.mask = this.shape_172.mask = this.shape_173.mask = this.shape_174.mask = this.shape_175.mask = this.shape_176.mask = this.shape_177.mask = this.shape_178.mask = this.shape_179.mask = this.shape_180.mask = this.shape_181.mask = this.shape_182.mask = this.shape_183.mask = this.shape_184.mask = this.shape_185.mask = this.shape_186.mask = this.shape_187.mask = this.shape_188.mask = this.shape_189.mask = this.shape_190.mask = this.shape_191.mask = this.shape_192.mask = this.shape_193.mask = this.shape_194.mask = this.shape_195.mask = this.shape_196.mask = this.shape_197.mask = this.shape_198.mask = this.shape_199.mask = this.shape_200.mask = this.shape_201.mask = this.shape_202.mask = this.shape_203.mask = this.shape_204.mask = this.shape_205.mask = this.shape_206.mask = this.shape_207.mask = this.shape_208.mask = this.shape_209.mask = this.shape_210.mask = this.shape_211.mask = this.shape_212.mask = this.shape_213.mask = this.shape_214.mask = this.shape_215.mask = this.shape_216.mask = this.shape_217.mask = this.shape_218.mask = this.shape_219.mask = this.shape_220.mask = this.shape_221.mask = this.shape_222.mask = this.shape_223.mask = this.shape_224.mask = this.shape_225.mask = this.shape_226.mask = this.shape_227.mask = this.shape_228.mask = this.shape_229.mask = this.shape_230.mask = this.shape_231.mask = this.shape_232.mask = this.shape_233.mask = this.shape_234.mask = this.shape_235.mask = this.shape_236.mask = this.shape_237.mask = this.shape_238.mask = this.shape_239.mask = this.shape_240.mask = this.shape_241.mask = this.shape_242.mask = this.shape_243.mask = this.shape_244.mask = this.shape_245.mask = this.shape_246.mask = this.shape_247.mask = this.shape_248.mask = this.shape_249.mask = this.shape_250.mask = this.shape_251.mask = this.shape_252.mask = this.shape_253.mask = this.shape_254.mask = this.shape_255.mask = this.shape_256.mask = this.shape_257.mask = this.shape_258.mask = this.shape_259.mask = this.shape_260.mask = this.shape_261.mask = this.shape_262.mask = this.shape_263.mask = this.shape_264.mask = this.shape_265.mask = this.shape_266.mask = this.shape_267.mask = this.shape_268.mask = this.shape_269.mask = this.shape_270.mask = this.shape_271.mask = this.shape_272.mask = this.shape_273.mask = this.shape_274.mask = this.shape_275.mask = this.shape_276.mask = this.shape_277.mask = this.shape_278.mask = mask;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_102}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_108}]},1).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_114}]},1).to({state:[{t:this.shape_115}]},1).to({state:[{t:this.shape_116}]},1).to({state:[{t:this.shape_117}]},1).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_119}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_121}]},1).to({state:[{t:this.shape_122}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_124}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_126}]},1).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_128}]},1).to({state:[{t:this.shape_129}]},1).to({state:[{t:this.shape_130}]},1).to({state:[{t:this.shape_131}]},1).to({state:[{t:this.shape_132}]},1).to({state:[{t:this.shape_133}]},1).to({state:[{t:this.shape_134}]},1).to({state:[{t:this.shape_135}]},1).to({state:[{t:this.shape_136}]},1).to({state:[{t:this.shape_137}]},1).to({state:[{t:this.shape_138}]},1).to({state:[{t:this.shape_139}]},1).to({state:[{t:this.shape_140}]},1).to({state:[{t:this.shape_141}]},1).to({state:[{t:this.shape_142}]},1).to({state:[{t:this.shape_143}]},1).to({state:[{t:this.shape_144}]},1).to({state:[{t:this.shape_145}]},1).to({state:[{t:this.shape_146}]},1).to({state:[{t:this.shape_147}]},1).to({state:[{t:this.shape_148}]},1).to({state:[{t:this.shape_149}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_151}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_153}]},1).to({state:[{t:this.shape_154}]},1).to({state:[{t:this.shape_155}]},1).to({state:[{t:this.shape_156}]},1).to({state:[{t:this.shape_157}]},1).to({state:[{t:this.shape_158}]},1).to({state:[{t:this.shape_159}]},1).to({state:[{t:this.shape_160}]},1).to({state:[{t:this.shape_161}]},1).to({state:[{t:this.shape_162}]},1).to({state:[{t:this.shape_163}]},1).to({state:[{t:this.shape_164}]},1).to({state:[{t:this.shape_165}]},1).to({state:[{t:this.shape_166}]},1).to({state:[{t:this.shape_167}]},1).to({state:[{t:this.shape_168}]},1).to({state:[{t:this.shape_169}]},1).to({state:[{t:this.shape_170}]},1).to({state:[{t:this.shape_171}]},1).to({state:[{t:this.shape_172}]},1).to({state:[{t:this.shape_173}]},1).to({state:[{t:this.shape_174}]},1).to({state:[{t:this.shape_175}]},1).to({state:[{t:this.shape_176}]},1).to({state:[{t:this.shape_177}]},1).to({state:[{t:this.shape_178}]},1).to({state:[{t:this.shape_179}]},1).to({state:[{t:this.shape_180}]},1).to({state:[{t:this.shape_181}]},1).to({state:[{t:this.shape_182}]},1).to({state:[{t:this.shape_183}]},1).to({state:[{t:this.shape_184}]},1).to({state:[{t:this.shape_185}]},1).to({state:[{t:this.shape_186}]},1).to({state:[{t:this.shape_187}]},1).to({state:[{t:this.shape_188}]},1).to({state:[{t:this.shape_189}]},1).to({state:[{t:this.shape_190}]},1).to({state:[{t:this.shape_191}]},1).to({state:[{t:this.shape_192}]},1).to({state:[{t:this.shape_193}]},1).to({state:[{t:this.shape_194}]},1).to({state:[{t:this.shape_195}]},1).to({state:[{t:this.shape_196}]},1).to({state:[{t:this.shape_197}]},1).to({state:[{t:this.shape_198}]},1).to({state:[{t:this.shape_199}]},1).to({state:[{t:this.shape_200}]},1).to({state:[{t:this.shape_201}]},1).to({state:[{t:this.shape_202}]},1).to({state:[{t:this.shape_203}]},1).to({state:[{t:this.shape_204}]},1).to({state:[{t:this.shape_205}]},1).to({state:[{t:this.shape_206}]},1).to({state:[{t:this.shape_207}]},1).to({state:[{t:this.shape_208}]},1).to({state:[{t:this.shape_209}]},1).to({state:[{t:this.shape_210}]},1).to({state:[{t:this.shape_211}]},1).to({state:[{t:this.shape_212}]},1).to({state:[{t:this.shape_213}]},1).to({state:[{t:this.shape_214}]},1).to({state:[{t:this.shape_215}]},1).to({state:[{t:this.shape_216}]},1).to({state:[{t:this.shape_217}]},1).to({state:[{t:this.shape_218}]},1).to({state:[{t:this.shape_219}]},1).to({state:[{t:this.shape_220}]},1).to({state:[{t:this.shape_221}]},1).to({state:[{t:this.shape_222}]},1).to({state:[{t:this.shape_223}]},1).to({state:[{t:this.shape_224}]},1).to({state:[{t:this.shape_223}]},1).to({state:[{t:this.shape_222}]},1).to({state:[{t:this.shape_221}]},1).to({state:[{t:this.shape_220}]},1).to({state:[{t:this.shape_219}]},1).to({state:[{t:this.shape_218}]},1).to({state:[{t:this.shape_217}]},1).to({state:[{t:this.shape_216}]},1).to({state:[{t:this.shape_215}]},1).to({state:[{t:this.shape_214}]},1).to({state:[{t:this.shape_213}]},1).to({state:[{t:this.shape_212}]},1).to({state:[{t:this.shape_211}]},1).to({state:[{t:this.shape_210}]},1).to({state:[{t:this.shape_209}]},1).to({state:[{t:this.shape_208}]},1).to({state:[{t:this.shape_207}]},1).to({state:[{t:this.shape_206}]},1).to({state:[{t:this.shape_205}]},1).to({state:[{t:this.shape_204}]},1).to({state:[{t:this.shape_203}]},1).to({state:[{t:this.shape_202}]},1).to({state:[{t:this.shape_201}]},1).to({state:[{t:this.shape_200}]},1).to({state:[{t:this.shape_199}]},1).to({state:[{t:this.shape_198}]},1).to({state:[{t:this.shape_197}]},1).to({state:[{t:this.shape_196}]},1).to({state:[{t:this.shape_195}]},1).to({state:[{t:this.shape_194}]},1).to({state:[{t:this.shape_193}]},1).to({state:[{t:this.shape_192}]},1).to({state:[{t:this.shape_191}]},1).to({state:[{t:this.shape_190}]},1).to({state:[{t:this.shape_189}]},1).to({state:[{t:this.shape_188}]},1).to({state:[{t:this.shape_187}]},1).to({state:[{t:this.shape_186}]},1).to({state:[{t:this.shape_185}]},1).to({state:[{t:this.shape_184}]},1).to({state:[{t:this.shape_183}]},1).to({state:[{t:this.shape_182}]},1).to({state:[{t:this.shape_181}]},1).to({state:[{t:this.shape_180}]},1).to({state:[{t:this.shape_179}]},1).to({state:[{t:this.shape_178}]},1).to({state:[{t:this.shape_177}]},1).to({state:[{t:this.shape_176}]},1).to({state:[{t:this.shape_175}]},1).to({state:[{t:this.shape_174}]},1).to({state:[{t:this.shape_173}]},1).to({state:[{t:this.shape_172}]},1).to({state:[{t:this.shape_171}]},1).to({state:[{t:this.shape_170}]},1).to({state:[{t:this.shape_169}]},1).to({state:[{t:this.shape_168}]},1).to({state:[{t:this.shape_167}]},1).to({state:[{t:this.shape_166}]},1).to({state:[{t:this.shape_165}]},1).to({state:[{t:this.shape_164}]},1).to({state:[{t:this.shape_163}]},1).to({state:[{t:this.shape_162}]},1).to({state:[{t:this.shape_161}]},1).to({state:[{t:this.shape_160}]},1).to({state:[{t:this.shape_159}]},1).to({state:[{t:this.shape_158}]},1).to({state:[{t:this.shape_157}]},1).to({state:[{t:this.shape_156}]},1).to({state:[{t:this.shape_155}]},1).to({state:[{t:this.shape_154}]},1).to({state:[{t:this.shape_153}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_151}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_149}]},1).to({state:[{t:this.shape_148}]},1).to({state:[{t:this.shape_147}]},1).to({state:[{t:this.shape_146}]},1).to({state:[{t:this.shape_145}]},1).to({state:[{t:this.shape_144}]},1).to({state:[{t:this.shape_143}]},1).to({state:[{t:this.shape_142}]},1).to({state:[{t:this.shape_141}]},1).to({state:[{t:this.shape_140}]},1).to({state:[{t:this.shape_225}]},1).to({state:[{t:this.shape_226}]},1).to({state:[{t:this.shape_227}]},1).to({state:[{t:this.shape_228}]},1).to({state:[{t:this.shape_229}]},1).to({state:[{t:this.shape_230}]},1).to({state:[{t:this.shape_231}]},1).to({state:[{t:this.shape_232}]},1).to({state:[{t:this.shape_233}]},1).to({state:[{t:this.shape_234}]},1).to({state:[{t:this.shape_235}]},1).to({state:[{t:this.shape_236}]},1).to({state:[{t:this.shape_237}]},1).to({state:[{t:this.shape_238}]},1).to({state:[{t:this.shape_239}]},1).to({state:[{t:this.shape_240}]},1).to({state:[{t:this.shape_241}]},1).to({state:[{t:this.shape_242}]},1).to({state:[{t:this.shape_243}]},1).to({state:[{t:this.shape_244}]},1).to({state:[{t:this.shape_245}]},1).to({state:[{t:this.shape_246}]},1).to({state:[{t:this.shape_247}]},1).to({state:[{t:this.shape_248}]},1).to({state:[{t:this.shape_249}]},1).to({state:[{t:this.shape_250}]},1).to({state:[{t:this.shape_251}]},1).to({state:[{t:this.shape_252}]},1).to({state:[{t:this.shape_253}]},1).to({state:[{t:this.shape_254}]},1).to({state:[{t:this.shape_255}]},1).to({state:[{t:this.shape_256}]},1).to({state:[{t:this.shape_257}]},1).to({state:[{t:this.shape_258}]},1).to({state:[{t:this.shape_259}]},1).to({state:[{t:this.shape_260}]},1).to({state:[{t:this.shape_261}]},1).to({state:[{t:this.shape_262}]},1).to({state:[{t:this.shape_263}]},1).to({state:[{t:this.shape_264}]},1).to({state:[{t:this.shape_265}]},1).to({state:[{t:this.shape_266}]},1).to({state:[{t:this.shape_267}]},1).to({state:[{t:this.shape_268}]},1).to({state:[{t:this.shape_269}]},1).to({state:[{t:this.shape_270}]},1).to({state:[{t:this.shape_271}]},1).to({state:[{t:this.shape_272}]},1).to({state:[{t:this.shape_273}]},1).to({state:[{t:this.shape_274}]},1).to({state:[{t:this.shape_275}]},1).to({state:[{t:this.shape_276}]},1).to({state:[{t:this.shape_277}]},1).to({state:[{t:this.shape_278}]},1).wait(1));

	// stroke
	this.shape_279 = new cjs.Shape();
	this.shape_279.graphics.f("#FFD56C").s().p("AgDAKIAAgTIAIAAIAAATg");
	this.shape_279.setTransform(-0.6,229);

	this.shape_280 = new cjs.Shape();
	this.shape_280.graphics.f("#FFD56C").s().p("AgEAKIAAgTIAJAAIAAATg");
	this.shape_280.setTransform(-0.5,229);

	this.shape_281 = new cjs.Shape();
	this.shape_281.graphics.f("#FFD56C").s().p("AgFAKIAAgSIALgBIAAATg");
	this.shape_281.setTransform(-0.4,229);

	this.shape_282 = new cjs.Shape();
	this.shape_282.graphics.f("#FFD56C").s().p("AgIAKIAAgSIAQgBIAAATg");
	this.shape_282.setTransform(-0.2,228.9);

	this.shape_283 = new cjs.Shape();
	this.shape_283.graphics.f("#FFD56C").s().p("AgLALIAAgSIAWgDQABAKAAALg");
	this.shape_283.setTransform(0.1,228.9);

	this.shape_284 = new cjs.Shape();
	this.shape_284.graphics.f("#FFD56C").s().p("AgPALIAAgSQASgBAMgCIABAVg");
	this.shape_284.setTransform(0.5,228.8);

	this.shape_285 = new cjs.Shape();
	this.shape_285.graphics.f("#FFD56C").s().p("AgUAMIAAgSQAYgBAQgEQACALgCAMg");
	this.shape_285.setTransform(1,228.7);

	this.shape_286 = new cjs.Shape();
	this.shape_286.graphics.f("#FFD56C").s().p("AgaANIAAgSQAggCAUgFQACAMgCANg");
	this.shape_286.setTransform(1.6,228.6);

	this.shape_287 = new cjs.Shape();
	this.shape_287.graphics.f("#FFD56C").s().p("AghAOIAAgSQApgDAYgGQADANgDAOg");
	this.shape_287.setTransform(2.3,228.5);

	this.shape_288 = new cjs.Shape();
	this.shape_288.graphics.f("#FFD56C").s().p("AgoAPIAAgSQAygDAdgIQAEAOgDAPg");
	this.shape_288.setTransform(3.1,228.4);

	this.shape_289 = new cjs.Shape();
	this.shape_289.graphics.f("#FFD56C").s().p("AgxARIAAgSQA9gFAjgKQAFARgEAQg");
	this.shape_289.setTransform(4,228.2);

	this.shape_290 = new cjs.Shape();
	this.shape_290.graphics.f("#FFD56C").s().p("Ag6ASIAAgSQBJgFAqgMQAFASgFARg");
	this.shape_290.setTransform(4.9,228.1);

	this.shape_291 = new cjs.Shape();
	this.shape_291.graphics.f("#FFD56C").s().p("AhFAUIAAgUQBXgEAwgPQAHAUgGATg");
	this.shape_291.setTransform(6,227.9);

	this.shape_292 = new cjs.Shape();
	this.shape_292.graphics.f("#FFD56C").s().p("AhQAWIAAgUQBlgGA4gRQAHAWgGAVg");
	this.shape_292.setTransform(7.1,227.7);

	this.shape_293 = new cjs.Shape();
	this.shape_293.graphics.f("#FFD56C").s().p("AhdAYIAAgVQB1gGBBgUQAIAYgHAXg");
	this.shape_293.setTransform(8.4,227.5);

	this.shape_294 = new cjs.Shape();
	this.shape_294.graphics.f("#FFD56C").s().p("AhpAaIAAgVQCFgIBJgWQAKAagJAZg");
	this.shape_294.setTransform(9.7,227.3);

	this.shape_295 = new cjs.Shape();
	this.shape_295.graphics.f("#FFD56C").s().p("Ah3AcIAAgUQCXgKBTgZQALAdgKAag");
	this.shape_295.setTransform(11.1,227.1);

	this.shape_296 = new cjs.Shape();
	this.shape_296.graphics.f("#FFD56C").s().p("AiHAeIAAgUQCrgLBcgcQANAfgLAcg");
	this.shape_296.setTransform(12.6,226.8);

	this.shape_297 = new cjs.Shape();
	this.shape_297.graphics.f("#FFD56C").s().p("AiWAhIAAgUQC/gNBnggQAOAjgMAeg");
	this.shape_297.setTransform(14.2,226.6);

	this.shape_298 = new cjs.Shape();
	this.shape_298.graphics.f("#FFD56C").s().p("AinAkIAAgVQDUgPBygjQAQAmgOAhg");
	this.shape_298.setTransform(15.9,226.3);

	this.shape_299 = new cjs.Shape();
	this.shape_299.graphics.f("#FFD56C").s().p("Ai5AnIAAgUQDrgSB+gnQASApgPAkg");
	this.shape_299.setTransform(17.7,226);

	this.shape_300 = new cjs.Shape();
	this.shape_300.graphics.f("#FFD56C").s().p("AjLAqIAAgVQEDgTCKgqQATAsgQAmg");
	this.shape_300.setTransform(19.5,225.7);

	this.shape_301 = new cjs.Shape();
	this.shape_301.graphics.f("#FFD56C").s().p("AjfAtIAAgVQEdgVCXgvQAVAwgTApg");
	this.shape_301.setTransform(21.5,225.3);

	this.shape_302 = new cjs.Shape();
	this.shape_302.graphics.f("#FFD56C").s().p("AjzAwIAAgVQE2gXClgzQAXA0gUArg");
	this.shape_302.setTransform(23.6,225);

	this.shape_303 = new cjs.Shape();
	this.shape_303.graphics.f("#FFD56C").s().p("AkJAzIAAgUQFSgZCzg4QAZA3gWAug");
	this.shape_303.setTransform(25.7,224.6);

	this.shape_304 = new cjs.Shape();
	this.shape_304.graphics.f("#FFD56C").s().p("AkfA3IAAgUQFugcDCg9QAcA8gYAxg");
	this.shape_304.setTransform(27.9,224.3);

	this.shape_305 = new cjs.Shape();
	this.shape_305.graphics.f("#FFD56C").s().p("Ak2A7IAAgVQGMgeDRhCQAeBAgaA1g");
	this.shape_305.setTransform(30.3,223.9);

	this.shape_306 = new cjs.Shape();
	this.shape_306.graphics.f("#FFD56C").s().p("AlOA+IAAgUQGrggDhhHQAgBEgcA3g");
	this.shape_306.setTransform(32.7,223.5);

	this.shape_307 = new cjs.Shape();
	this.shape_307.graphics.f("#FFD56C").s().p("AlnBDIAAgVQHKgjDyhMQAjBIgeA8g");
	this.shape_307.setTransform(35.2,223.1);

	this.shape_308 = new cjs.Shape();
	this.shape_308.graphics.f("#FFD56C").s().p("AmBBHIAAgVQHsglEDhTQAlBOghA/g");
	this.shape_308.setTransform(37.8,222.6);

	this.shape_309 = new cjs.Shape();
	this.shape_309.graphics.f("#FFD56C").s().p("AmbBLIAAgVQIOgnEVhZQAnBSgjBDg");
	this.shape_309.setTransform(40.5,222.2);

	this.shape_310 = new cjs.Shape();
	this.shape_310.graphics.f("#FFD56C").s().p("Am3BPIAAgVQIxgqEoheQAqBXglBGg");
	this.shape_310.setTransform(43.3,221.7);

	this.shape_311 = new cjs.Shape();
	this.shape_311.graphics.f("#FFD56C").s().p("AnUBUIAAgVQJWgtE6hlQAtBdgnBKg");
	this.shape_311.setTransform(46.2,221.2);

	this.shape_312 = new cjs.Shape();
	this.shape_312.graphics.f("#FFD56C").s().p("AnxBZIAAgWQJ8gvFOhrQAvBhgqBPg");
	this.shape_312.setTransform(49.1,220.8);

	this.shape_313 = new cjs.Shape();
	this.shape_313.graphics.f("#FFD56C").s().p("AoPBeIAAgWQKigzFjhxQAyBngsBTg");
	this.shape_313.setTransform(52.2,220.3);

	this.shape_314 = new cjs.Shape();
	this.shape_314.graphics.f("#FFD56C").s().p("AouBiIAAgUQLKg3F3h5QA2BugvBWg");
	this.shape_314.setTransform(55.3,219.7);

	this.shape_315 = new cjs.Shape();
	this.shape_315.graphics.f("#FFD56C").s().p("ApPBoIAAgWQL0g4GNiBQA4BzgxBcg");
	this.shape_315.setTransform(58.6,219.2);

	this.shape_316 = new cjs.Shape();
	this.shape_316.graphics.f("#FFD56C").s().p("ApwBtIAAgVQMeg8GjiIQA8B5g1Bgg");
	this.shape_316.setTransform(61.9,218.6);

	this.shape_317 = new cjs.Shape();
	this.shape_317.graphics.f("#FFD56C").s().p("AqSBzIAAgWQNKg/G5iPQA/B/g3Blg");
	this.shape_317.setTransform(65.3,218.1);

	this.shape_318 = new cjs.Shape();
	this.shape_318.graphics.f("#FFD56C").s().p("Aq1B4IAAgWQN3hCHQiXQBDCGg7Bpg");
	this.shape_318.setTransform(68.9,217.5);

	this.shape_319 = new cjs.Shape();
	this.shape_319.graphics.f("#FFD56C").s().p("ArZB+IAAgWQOlhGHoifQBGCMg9Bvg");
	this.shape_319.setTransform(72.5,216.9);

	this.shape_320 = new cjs.Shape();
	this.shape_320.graphics.f("#FFD56C").s().p("Ar9CDIAAgVQPThJIBioQBJCUhAByg");
	this.shape_320.setTransform(76.2,216.3);

	this.shape_321 = new cjs.Shape();
	this.shape_321.graphics.f("#FFD56C").s().p("AsjCKIAAgWQQEhNIaiwQBNCahEB5g");
	this.shape_321.setTransform(80,215.6);

	this.shape_322 = new cjs.Shape();
	this.shape_322.graphics.f("#FFD56C").s().p("AtJCQIAAgWQQ1hRIzi4QBRChhHB+g");
	this.shape_322.setTransform(83.8,215);

	this.shape_323 = new cjs.Shape();
	this.shape_323.graphics.f("#FFD56C").s().p("AtxCWIAAgWQRohVJNjAQBVCohKCDg");
	this.shape_323.setTransform(87.8,214.3);

	this.shape_324 = new cjs.Shape();
	this.shape_324.graphics.f("#FFD56C").s().p("AuZCcIAAgVQSbhZJpjKQBYCwhNCIg");
	this.shape_324.setTransform(91.9,213.7);

	this.shape_325 = new cjs.Shape();
	this.shape_325.graphics.f("#FFD56C").s().p("AvCCjIAAgWQTQhdKEjSQBcC2hQCPg");
	this.shape_325.setTransform(96,213);

	this.shape_326 = new cjs.Shape();
	this.shape_326.graphics.f("#FFD56C").s().p("AvsCqIAAgWQUGhhKgjcQBhC+hVCVg");
	this.shape_326.setTransform(100.2,212.3);

	this.shape_327 = new cjs.Shape();
	this.shape_327.graphics.f("#FFD56C").s().p("AwYCxIAAgWQU+hlK9jmQBkDHhYCag");
	this.shape_327.setTransform(104.6,211.5);

	this.shape_328 = new cjs.Shape();
	this.shape_328.graphics.f("#FFD56C").s().p("AxDC4IAAgXQV1hpLbjvQBpDPhcCgg");
	this.shape_328.setTransform(109,210.8);

	this.shape_329 = new cjs.Shape();
	this.shape_329.graphics.f("#FFD56C").s().p("AxwC/IAAgWQWvhuL4j5QBtDWhgCng");
	this.shape_329.setTransform(113.6,210.1);

	this.shape_330 = new cjs.Shape();
	this.shape_330.graphics.f("#FFD56C").s().p("AyeDGIAAgWQXqhyMXkDQBxDehjCtg");
	this.shape_330.setTransform(118.2,209.3);

	this.shape_331 = new cjs.Shape();
	this.shape_331.graphics.f("#FFD56C").s().p("AzNDOIAAgXQYmh3M2kNQB2DohoCzg");
	this.shape_331.setTransform(122.9,208.5);

	this.shape_332 = new cjs.Shape();
	this.shape_332.graphics.f("#FFD56C").s().p("Az8DWIAAgXQZih8NWkXQB7DwhsC6g");
	this.shape_332.setTransform(127.7,207.7);

	this.shape_333 = new cjs.Shape();
	this.shape_333.graphics.f("#FFD56C").s().p("A0tDdIAAgWQahiAN2kjQB/D5hvDAg");
	this.shape_333.setTransform(132.6,206.9);

	this.shape_334 = new cjs.Shape();
	this.shape_334.graphics.f("#FFD56C").s().p("A1eDlIAAgXQbgiEOXkuQCEECh0DHg");
	this.shape_334.setTransform(137.5,206.1);

	this.shape_335 = new cjs.Shape();
	this.shape_335.graphics.f("#FFD56C").s().p("A2RDtIAAgXQchiJO4k5QCJELh4DOg");
	this.shape_335.setTransform(142.6,205.2);

	this.shape_336 = new cjs.Shape();
	this.shape_336.graphics.f("#FFD56C").s().p("A20EEIAAgXQc/iIPjloQCGEhh2Dmg");
	this.shape_336.setTransform(146.2,203);

	this.shape_337 = new cjs.Shape();
	this.shape_337.graphics.f("#FFD56C").s().p("A3XEaIAAgWQddiGQNmXQCCE3hyD8g");
	this.shape_337.setTransform(149.7,200.7);

	this.shape_338 = new cjs.Shape();
	this.shape_338.graphics.f("#FFD56C").s().p("A36ExIAAgXQd7iEQ2nGQCAFNhwEUg");
	this.shape_338.setTransform(153.2,198.5);

	this.shape_339 = new cjs.Shape();
	this.shape_339.graphics.f("#FFD56C").s().p("A4dFHIAAgYQeZiARfn1QB8FihsErg");
	this.shape_339.setTransform(156.6,196.3);

	this.shape_340 = new cjs.Shape();
	this.shape_340.graphics.f("#FFD56C").s().p("A4+FcIAAgXQe1h/SIohQB5F2hqFBg");
	this.shape_340.setTransform(160,194.1);

	this.shape_341 = new cjs.Shape();
	this.shape_341.graphics.f("#FFD56C").s().p("A5gFyIAAgYQfTh8SvpPQB2GLhnFYg");
	this.shape_341.setTransform(163.4,192);

	this.shape_342 = new cjs.Shape();
	this.shape_342.graphics.f("#FFD56C").s().p("A6BGHIAAgYQfvh6TXp7QBzGghlFtg");
	this.shape_342.setTransform(166.7,189.9);

	this.shape_343 = new cjs.Shape();
	this.shape_343.graphics.f("#FFD56C").s().p("A6iGcIAAgYUAgMgB5AT9gKmQBwG0hiGDg");
	this.shape_343.setTransform(169.9,187.8);

	this.shape_344 = new cjs.Shape();
	this.shape_344.graphics.f("#FFD56C").s().p("A7CGwIAAgXUAgngB3AUkgLSQBtHIhgGYg");
	this.shape_344.setTransform(173.2,185.7);

	this.shape_345 = new cjs.Shape();
	this.shape_345.graphics.f("#FFD56C").s().p("A7iHFIAAgYUAhDgB1AVJgL8QBrHcheGtg");
	this.shape_345.setTransform(176.3,183.7);

	this.shape_346 = new cjs.Shape();
	this.shape_346.graphics.f("#FFD56C").s().p("A8BHZIAAgYUAhdgBzAVvgMmQBoHvhbHCg");
	this.shape_346.setTransform(179.5,181.7);

	this.shape_347 = new cjs.Shape();
	this.shape_347.graphics.f("#FFD56C").s().p("A8gHsIAAgXUAh4gByAWTgNOQBlIChYHVg");
	this.shape_347.setTransform(182.6,179.7);

	this.shape_348 = new cjs.Shape();
	this.shape_348.graphics.f("#FFD56C").s().p("A8/IAIAAgYUAiTgBvAW3gN4QBiIVhVHqg");
	this.shape_348.setTransform(185.6,177.8);

	this.shape_349 = new cjs.Shape();
	this.shape_349.graphics.f("#FFD56C").s().p("A9dITIAAgYUAitgBtAXbgOgQBfInhTH+g");
	this.shape_349.setTransform(188.6,175.8);

	this.shape_350 = new cjs.Shape();
	this.shape_350.graphics.f("#FFD56C").s().p("A96ImIAAgYUAjGgBrAX+gPIQBdI6hRIRg");
	this.shape_350.setTransform(191.6,173.9);

	this.shape_351 = new cjs.Shape();
	this.shape_351.graphics.f("#FFD56C").s().p("A+YI5IAAgYUAjfgBqAYigPvQBaJMhPIlg");
	this.shape_351.setTransform(194.5,172.1);

	this.shape_352 = new cjs.Shape();
	this.shape_352.graphics.f("#FFD56C").s().p("A+1JLIAAgYUAj4gBoAZEgQVQBXJdhMI4g");
	this.shape_352.setTransform(197.4,170.2);

	this.shape_353 = new cjs.Shape();
	this.shape_353.graphics.f("#FFD56C").s().p("A/RJdIAAgYUAkQgBmAZmgQ7QBUJvhKJKg");
	this.shape_353.setTransform(200.3,168.4);

	this.shape_354 = new cjs.Shape();
	this.shape_354.graphics.f("#FFD56C").s().p("A/tJvIAAgYUAkpgBlAaGgRgQBSKAhHJdg");
	this.shape_354.setTransform(203,166.6);

	this.shape_355 = new cjs.Shape();
	this.shape_355.graphics.f("#FFD56C").s().p("EggIAKBIAAgZUAlAgBiAangSGQBQKShGJvg");
	this.shape_355.setTransform(205.8,164.9);

	this.shape_356 = new cjs.Shape();
	this.shape_356.graphics.f("#FFD56C").s().p("EggkAKSIAAgYUAlYgBhAbHgSqQBNKihDKBg");
	this.shape_356.setTransform(208.5,163.1);

	this.shape_357 = new cjs.Shape();
	this.shape_357.graphics.f("#FFD56C").s().p("Egg+AKjIAAgYUAlugBgAbogTNQBKKyhBKTg");
	this.shape_357.setTransform(211.2,161.4);

	this.shape_358 = new cjs.Shape();
	this.shape_358.graphics.f("#FFD56C").s().p("EghZAK0IAAgZUAmFgBeAcHgTwQBILDg/Kkg");
	this.shape_358.setTransform(213.8,159.8);

	this.shape_359 = new cjs.Shape();
	this.shape_359.graphics.f("#FFD56C").s().p("EghyALEIAAgYUAmbgBdAclgUSQBGLTg9K0g");
	this.shape_359.setTransform(216.4,158.1);

	this.shape_360 = new cjs.Shape();
	this.shape_360.graphics.f("#FFD56C").s().p("EgiMALVIAAgZUAmxgBbAdEgU1QBDLjg7LGg");
	this.shape_360.setTransform(218.9,156.5);

	this.shape_361 = new cjs.Shape();
	this.shape_361.graphics.f("#FFD56C").s().p("EgilALlIAAgZUAnHgBZAdhgVXQBBLyg5LXg");
	this.shape_361.setTransform(221.4,154.9);

	this.shape_362 = new cjs.Shape();
	this.shape_362.graphics.f("#FFD56C").s().p("Egi9AL0IAAgYUAnbgBYAd/gV4QA/MCg3Lmg");
	this.shape_362.setTransform(223.9,153.3);

	this.shape_363 = new cjs.Shape();
	this.shape_363.graphics.f("#FFD56C").s().p("EgjVAMEIAAgZUAnwgBWAebgWYQA9MQg2L3g");
	this.shape_363.setTransform(226.3,151.8);

	this.shape_364 = new cjs.Shape();
	this.shape_364.graphics.f("#FFD56C").s().p("EgjtAMTIAAgZUAoFgBVAe3gW3QA6MfgzMGg");
	this.shape_364.setTransform(228.7,150.3);

	this.shape_365 = new cjs.Shape();
	this.shape_365.graphics.f("#FFD56C").s().p("EgkEAMiIAAgZUAoZgBUAfSgXWQA5MtgyMWg");
	this.shape_365.setTransform(231,148.8);

	this.shape_366 = new cjs.Shape();
	this.shape_366.graphics.f("#FFD56C").s().p("EgkbAMwIAAgYUAosgBSAfugX1QA3M7gwMkg");
	this.shape_366.setTransform(233.3,147.3);

	this.shape_367 = new cjs.Shape();
	this.shape_367.graphics.f("#FFD56C").s().p("EgkyAM/IAAgZUApAgBRAgJgYTQA0NJguM0g");
	this.shape_367.setTransform(235.5,145.9);

	this.shape_368 = new cjs.Shape();
	this.shape_368.graphics.f("#FFD56C").s().p("EglIANNIAAgZUApTgBQAgjgYwQAyNXgsNCg");
	this.shape_368.setTransform(237.7,144.5);

	this.shape_369 = new cjs.Shape();
	this.shape_369.graphics.f("#FFD56C").s().p("EgldANaIAAgZUAplgBOAg8gZMQAxNkgrNPg");
	this.shape_369.setTransform(239.9,143.1);

	this.shape_370 = new cjs.Shape();
	this.shape_370.graphics.f("#FFD56C").s().p("EglyANoIAAgaUAp3gBMAhWgZpQAuNxgpNeg");
	this.shape_370.setTransform(242,141.8);

	this.shape_371 = new cjs.Shape();
	this.shape_371.graphics.f("#FFD56C").s().p("EgmHAN1IAAgZUAqJgBLAhugaFQAtN+gnNrg");
	this.shape_371.setTransform(244.1,140.4);

	this.shape_372 = new cjs.Shape();
	this.shape_372.graphics.f("#FFD56C").s().p("EgmbAOCIAAgZUAqbgBKAiGgagQArOKgmN5g");
	this.shape_372.setTransform(246.1,139.1);

	this.shape_373 = new cjs.Shape();
	this.shape_373.graphics.f("#FFD56C").s().p("EgmvAOPIAAgaUAqsgBIAidga7QApOXgkOGg");
	this.shape_373.setTransform(248.1,137.9);

	this.shape_374 = new cjs.Shape();
	this.shape_374.graphics.f("#FFD56C").s().p("EgnDAObIAAgZUAq9gBIAi1gbUQAnOigjOTg");
	this.shape_374.setTransform(250,136.6);

	this.shape_375 = new cjs.Shape();
	this.shape_375.graphics.f("#FFD56C").s().p("EgnWAOnIAAgZUArNgBGAjMgbvQAlOvghOfg");
	this.shape_375.setTransform(251.9,135.4);

	this.shape_376 = new cjs.Shape();
	this.shape_376.graphics.f("#FFD56C").s().p("EgnoAOzIAAgZUArdgBGAjhgcGQAkO6gfOrg");
	this.shape_376.setTransform(253.8,134.2);

	this.shape_377 = new cjs.Shape();
	this.shape_377.graphics.f("#FFD56C").s().p("Egn6AO/IAAgaUArsgBEAj3gcfQAjPGgeO3g");
	this.shape_377.setTransform(255.6,133.1);

	this.shape_378 = new cjs.Shape();
	this.shape_378.graphics.f("#FFD56C").s().p("EgoMAPKIAAgZUAr7gBDAkNgc3QAgPQgcPDg");
	this.shape_378.setTransform(257.4,131.9);

	this.shape_379 = new cjs.Shape();
	this.shape_379.graphics.f("#FFD56C").s().p("EgodAPVIAAgZUAsKgBCAkhgdOQAfPbgbPOg");
	this.shape_379.setTransform(259.1,130.8);

	this.shape_380 = new cjs.Shape();
	this.shape_380.graphics.f("#FFD56C").s().p("EgouAPgIAAgaUAsZgBBAk1gdkQAdPlgaPag");
	this.shape_380.setTransform(260.8,129.8);

	this.shape_381 = new cjs.Shape();
	this.shape_381.graphics.f("#FFD56C").s().p("Ego/APqIAAgZUAsogBAAlIgd6QAcPvgZPkg");
	this.shape_381.setTransform(262.4,128.7);

	this.shape_382 = new cjs.Shape();
	this.shape_382.graphics.f("#FFD56C").s().p("EgpPAP1IAAgaUAs1gA/AlcgeQQAaP6gXPvg");
	this.shape_382.setTransform(264,127.7);

	this.shape_383 = new cjs.Shape();
	this.shape_383.graphics.f("#FFD56C").s().p("EgpeAP/IAAgaUAtCgA+AlugelQAZQDgWP6g");
	this.shape_383.setTransform(265.6,126.7);

	this.shape_384 = new cjs.Shape();
	this.shape_384.graphics.f("#FFD56C").s().p("EgptAQIIAAgZUAtPgA+AmAge4QAYQMgVQDg");
	this.shape_384.setTransform(267.1,125.7);

	this.shape_385 = new cjs.Shape();
	this.shape_385.graphics.f("#FFD56C").s().p("Egp8AQSIAAgaUAtcgA8AmRgfNQAXQWgUQNg");
	this.shape_385.setTransform(268.6,124.8);

	this.shape_386 = new cjs.Shape();
	this.shape_386.graphics.f("#FFD56C").s().p("EgqLAQbIAAgaUAtpgA7AmigfgQAVQfgSQWg");
	this.shape_386.setTransform(270,123.9);

	this.shape_387 = new cjs.Shape();
	this.shape_387.graphics.f("#FFD56C").s().p("EgqYAQkIAAgaUAt0gA7AmzgfyQAUQngSQgg");
	this.shape_387.setTransform(271.4,123);

	this.shape_388 = new cjs.Shape();
	this.shape_388.graphics.f("#FFD56C").s().p("EgqmAQsIAAgZUAuAgA6AnDggEQATQvgRQog");
	this.shape_388.setTransform(272.7,122.1);

	this.shape_389 = new cjs.Shape();
	this.shape_389.graphics.f("#FFD56C").s().p("EgqzAQ1IAAgaUAuLgA5AnTggWQARQ3gPQyg");
	this.shape_389.setTransform(274,121.3);

	this.shape_390 = new cjs.Shape();
	this.shape_390.graphics.f("#FFD56C").s().p("Egq/AQ9IAAgaUAuWgA4AnhggnQAQQ/gOQ6g");
	this.shape_390.setTransform(275.3,120.5);

	this.shape_391 = new cjs.Shape();
	this.shape_391.graphics.f("#FFD56C").s().p("EgrMAREIAAgZUAuhgA4Anvgg2QAQRGgORBg");
	this.shape_391.setTransform(276.5,119.7);

	this.shape_392 = new cjs.Shape();
	this.shape_392.graphics.f("#FFD56C").s().p("EgrXARMIAAgaUAuqgA2An+ghHQAOROgMRJg");
	this.shape_392.setTransform(277.7,119);

	this.shape_393 = new cjs.Shape();
	this.shape_393.graphics.f("#FFD56C").s().p("EgrjARTIAAgaUAu1gA2AoLghVQANRUgMRRg");
	this.shape_393.setTransform(278.8,118.2);

	this.shape_394 = new cjs.Shape();
	this.shape_394.graphics.f("#FFD56C").s().p("EgruARaIAAgaUAu+gA1AoYghkQAMRbgLRYg");
	this.shape_394.setTransform(279.9,117.5);

	this.shape_395 = new cjs.Shape();
	this.shape_395.graphics.f("#FFD56C").s().p("Egr4ARhIAAgaUAvGgA1AolghyQALRigKRfg");
	this.shape_395.setTransform(281,116.9);

	this.shape_396 = new cjs.Shape();
	this.shape_396.graphics.f("#FFD56C").s().p("EgsCARnIAAgaUAvPgA0Aowgh/QALRogJRlg");
	this.shape_396.setTransform(282,116.2);

	this.shape_397 = new cjs.Shape();
	this.shape_397.graphics.f("#FFD56C").s().p("EgsMARtIAAgaUAvYgAzAo8giNQAJRvgIRrg");
	this.shape_397.setTransform(282.9,115.6);

	this.shape_398 = new cjs.Shape();
	this.shape_398.graphics.f("#FFD56C").s().p("EgsVARzIAAgaUAvggAzApGgiYQAJRzgIRyg");
	this.shape_398.setTransform(283.8,115);

	this.shape_399 = new cjs.Shape();
	this.shape_399.graphics.f("#FFD56C").s().p("EgseAR5IAAgbUAvngAyApRgikQAIR6gHR3g");
	this.shape_399.setTransform(284.7,114.5);

	this.shape_400 = new cjs.Shape();
	this.shape_400.graphics.f("#FFD56C").s().p("EgsmAR+IAAgaUAvugAyApbgivQAHR+gGR9g");
	this.shape_400.setTransform(285.5,113.9);

	this.shape_401 = new cjs.Shape();
	this.shape_401.graphics.f("#FFD56C").s().p("EgsuASDIAAgaUAv1gAxAplgi6QAGSDgGSCg");
	this.shape_401.setTransform(286.3,113.4);

	this.shape_402 = new cjs.Shape();
	this.shape_402.graphics.f("#FFD56C").s().p("Egs1ASIIAAgbUAv7gAwAptgjEQAGSIgFSHg");
	this.shape_402.setTransform(287.1,113);

	this.shape_403 = new cjs.Shape();
	this.shape_403.graphics.f("#FFD56C").s().p("Egs8ASMIAAgaUAwBgAwAp2gjNQAFSLgFSMg");
	this.shape_403.setTransform(287.8,112.5);

	this.shape_404 = new cjs.Shape();
	this.shape_404.graphics.f("#FFD56C").s().p("EgtDASRIAAgbUAwHgAwAp+gjWQAESQgESRg");
	this.shape_404.setTransform(288.4,112.1);

	this.shape_405 = new cjs.Shape();
	this.shape_405.graphics.f("#FFD56C").s().p("EgtJASVIAAgbUAwMgAvAqFgjfQAESUgESVg");
	this.shape_405.setTransform(289.1,111.7);

	this.shape_406 = new cjs.Shape();
	this.shape_406.graphics.f("#FFD56C").s().p("EgtPASYIAAgaUAwSgAvAqLgjmQAESXgDSYg");
	this.shape_406.setTransform(289.6,111.3);

	this.shape_407 = new cjs.Shape();
	this.shape_407.graphics.f("#FFD56C").s().p("EgtUAScIAAgbUAwWgAvAqSgjtQADSbgDScg");
	this.shape_407.setTransform(290.2,111);

	this.shape_408 = new cjs.Shape();
	this.shape_408.graphics.f("#FFD56C").s().p("EgtZASfIAAgbUAwagAuAqYgj0QACSegCSfg");
	this.shape_408.setTransform(290.7,110.7);

	this.shape_409 = new cjs.Shape();
	this.shape_409.graphics.f("#FFD56C").s().p("EgteASiIAAgbUAwegAuAqegj6QABShgBSig");
	this.shape_409.setTransform(291.1,110.4);

	this.shape_410 = new cjs.Shape();
	this.shape_410.graphics.f("#FFD56C").s().p("EgtiASkIAAgaUAwigAuAqigkAQABSkgBSkg");
	this.shape_410.setTransform(291.5,110.1);

	this.shape_411 = new cjs.Shape();
	this.shape_411.graphics.f("#FFD56C").s().p("EgtlASnIAAgbUAwkgAuAqngkEQABSmgBSng");
	this.shape_411.setTransform(291.9,109.9);

	this.shape_412 = new cjs.Shape();
	this.shape_412.graphics.f("#FFD56C").s().p("EgtpASpIAAgbUAwogAtAqqgkJQABSogBSpg");
	this.shape_412.setTransform(292.2,109.7);

	this.shape_413 = new cjs.Shape();
	this.shape_413.graphics.f("#FFD56C").s().p("EgtrASqIAAgaUAwqgAtAqtgkMMAAAAlTg");
	this.shape_413.setTransform(292.5,109.5);

	this.shape_414 = new cjs.Shape();
	this.shape_414.graphics.f("#FFD56C").s().p("EgtuASsIAAgbUAwsgAtAqwgkPMAAAAlXg");
	this.shape_414.setTransform(292.7,109.4);

	this.shape_415 = new cjs.Shape();
	this.shape_415.graphics.f("#FFD56C").s().p("EgtwAStIAAgaUAwugAtAqygkSMAAAAlZg");
	this.shape_415.setTransform(292.9,109.2);

	this.shape_416 = new cjs.Shape();
	this.shape_416.graphics.f("#FFD56C").s().p("EgtxASuIAAgaUAwvgAtAq0gkUMAAAAlbg");
	this.shape_416.setTransform(293.1,109.1);

	this.shape_417 = new cjs.Shape();
	this.shape_417.graphics.f("#FFD56C").s().p("EgtyASvIAAgbUAwwgAsAq1gkWMAAAAldg");
	this.shape_417.setTransform(293.2,109.1);

	this.shape_418 = new cjs.Shape();
	this.shape_418.graphics.f("#FFD56C").s().p("EgtzASvIAAgaUAwxgAtAq2gkWMAAAAldg");
	this.shape_418.setTransform(293.2,109);

	this.shape_419 = new cjs.Shape();
	this.shape_419.graphics.f("#FFD56C").s().p("EgtzASvIAAgaUAwwgAtAq3gkWMAAAAldg");
	this.shape_419.setTransform(293.3,109);

	this.shape_420 = new cjs.Shape();
	this.shape_420.graphics.f("#FFD56C").s().p("EgtyAStIAAgaUAwtgAvAq4gkQMAAAAlZg");
	this.shape_420.setTransform(293.2,109.2);

	this.shape_421 = new cjs.Shape();
	this.shape_421.graphics.f("#FFD56C").s().p("EgtxASrIAAgaUAwrgAwAq4gkLMAAAAlVg");
	this.shape_421.setTransform(293.1,109.4);

	this.shape_422 = new cjs.Shape();
	this.shape_422.graphics.f("#FFD56C").s().p("EgtwASpIAAgbUAwogAxAq5gkFMAAAAlRg");
	this.shape_422.setTransform(293,109.7);

	this.shape_423 = new cjs.Shape();
	this.shape_423.graphics.f("#FFD56C").s().p("EgtvASnIAAgbUAwlgAyAq6gkAMAAAAlNg");
	this.shape_423.setTransform(292.9,109.9);

	this.shape_424 = new cjs.Shape();
	this.shape_424.graphics.f("#FFD56C").s().p("EgtuASlIAAgbUAwigA0Aq7gj6MAAAAlJg");
	this.shape_424.setTransform(292.8,110.1);

	this.shape_425 = new cjs.Shape();
	this.shape_425.graphics.f("#FFD56C").s().p("EgttASjIAAgbUAwggA1Aq7gj1MAAAAlFg");
	this.shape_425.setTransform(292.7,110.3);

	this.shape_426 = new cjs.Shape();
	this.shape_426.graphics.f("#FFD56C").s().p("EgtsASgIAAgaUAwdgA3Aq8gjvMAAAAlAg");
	this.shape_426.setTransform(292.6,110.5);

	this.shape_427 = new cjs.Shape();
	this.shape_427.graphics.f("#FFD56C").s().p("EgtrASeIAAgaUAwagA5Aq9gjoMAAAAk7g");
	this.shape_427.setTransform(292.5,110.7);

	this.shape_428 = new cjs.Shape();
	this.shape_428.graphics.f("#FFD56C").s().p("EgtqAScIAAgaUAwXgA6Aq+gjjMAAAAk3g");
	this.shape_428.setTransform(292.4,110.9);

	this.shape_429 = new cjs.Shape();
	this.shape_429.graphics.f("#FFD56C").s().p("EgtpASaIAAgbUAwVgA6Aq+gjeMAAAAkzg");
	this.shape_429.setTransform(292.3,111.2);

	this.shape_430 = new cjs.Shape();
	this.shape_430.graphics.f("#FFD56C").s().p("EgtoASYIAAgbUAwSgA8Aq/gjYMAAAAkvg");
	this.shape_430.setTransform(292.2,111.4);

	this.shape_431 = new cjs.Shape();
	this.shape_431.graphics.f("#FFD56C").s().p("EgtnASWIAAgbUAwPgA+ArAgjSMAAAAkrg");
	this.shape_431.setTransform(292.1,111.6);

	this.shape_432 = new cjs.Shape();
	this.shape_432.graphics.f("#FFD56C").s().p("EgtmASUIAAgbUAwNgA/ArAgjNMAAAAkng");
	this.shape_432.setTransform(292,111.8);

	this.shape_433 = new cjs.Shape();
	this.shape_433.graphics.f("#FFD56C").s().p("EgtlASRIAAgaUAwJgBBArCgjHMAAAAkig");
	this.shape_433.setTransform(291.9,112);

	this.shape_434 = new cjs.Shape();
	this.shape_434.graphics.f("#FFD56C").s().p("EgtkASPIAAgaUAwHgBDArCgjAMAAAAkdg");
	this.shape_434.setTransform(291.8,112.2);

	this.shape_435 = new cjs.Shape();
	this.shape_435.graphics.f("#FFD56C").s().p("EgtjASNIAAgaUAwEgBEArDgi7MAAAAkZg");
	this.shape_435.setTransform(291.7,112.4);

	this.shape_436 = new cjs.Shape();
	this.shape_436.graphics.f("#FFD56C").s().p("EgtiASLIAAgbUAwCgBFArDgi1MAAAAkVg");
	this.shape_436.setTransform(291.6,112.7);

	this.shape_437 = new cjs.Shape();
	this.shape_437.graphics.f("#FFD56C").s().p("EgthASJIAAgbUAv+gBGArFgiwMAAAAkRg");
	this.shape_437.setTransform(291.5,112.9);

	this.shape_438 = new cjs.Shape();
	this.shape_438.graphics.f("#FFD56C").s().p("EgtgASHIAAgbUAv8gBIArFgiqMAAAAkNg");
	this.shape_438.setTransform(291.4,113.1);

	this.shape_439 = new cjs.Shape();
	this.shape_439.graphics.f("#FFD56C").s().p("EgtfASFIAAgbUAv5gBJArGgilMAAAAkJg");
	this.shape_439.setTransform(291.3,113.3);

	this.shape_440 = new cjs.Shape();
	this.shape_440.graphics.f("#FFD56C").s().p("EgteASCIAAgaUAv3gBLArGgieMAAAAkDg");
	this.shape_440.setTransform(291.2,113.5);

	this.shape_441 = new cjs.Shape();
	this.shape_441.graphics.f("#FFD56C").s().p("EgtdASAIAAgaUAvzgBMArIgiZMAAAAj/g");
	this.shape_441.setTransform(291.1,113.7);

	this.shape_442 = new cjs.Shape();
	this.shape_442.graphics.f("#FFD56C").s().p("EgtcAR+IAAgaUAvxgBOArIgiTMAAAAj7g");
	this.shape_442.setTransform(291,113.9);

	this.shape_443 = new cjs.Shape();
	this.shape_443.graphics.f("#FFD56C").s().p("EgtbAR8IAAgbUAvugBOArJgiOMAAAAj3g");
	this.shape_443.setTransform(290.9,114.2);

	this.shape_444 = new cjs.Shape();
	this.shape_444.graphics.f("#FFD56C").s().p("EgtaAR6IAAgbUAvsgBQArJgiIMAAAAjzg");
	this.shape_444.setTransform(290.8,114.4);

	this.shape_445 = new cjs.Shape();
	this.shape_445.graphics.f("#FFD56C").s().p("EgtZAR4IAAgbUAvogBSArLgiCMAAAAjvg");
	this.shape_445.setTransform(290.7,114.6);

	this.shape_446 = new cjs.Shape();
	this.shape_446.graphics.f("#FFD56C").s().p("EgtYAR2IAAgbUAvmgBTArLgh9MAAAAjrg");
	this.shape_446.setTransform(290.6,114.8);

	this.shape_447 = new cjs.Shape();
	this.shape_447.graphics.f("#FFD56C").s().p("EgtXARzIAAgaUAvjgBUArMgh4MAAAAjmg");
	this.shape_447.setTransform(290.5,115);

	this.shape_448 = new cjs.Shape();
	this.shape_448.graphics.f("#FFD56C").s().p("EgtWARxIAAgaUAvggBWArNghxMAAAAjhg");
	this.shape_448.setTransform(290.4,115.2);

	this.shape_449 = new cjs.Shape();
	this.shape_449.graphics.f("#FFD56C").s().p("EgtVARvIAAgaUAvdgBYArOghrMAAAAjdg");
	this.shape_449.setTransform(290.3,115.4);

	this.shape_450 = new cjs.Shape();
	this.shape_450.graphics.f("#FFD56C").s().p("EgtUARtIAAgbUAvbgBZArOghlMAAAAjZg");
	this.shape_450.setTransform(290.2,115.7);

	this.shape_451 = new cjs.Shape();
	this.shape_451.graphics.f("#FFD56C").s().p("EgtTARrIAAgbUAvYgBaArPghgMAAAAjVg");
	this.shape_451.setTransform(290.1,115.9);

	this.shape_452 = new cjs.Shape();
	this.shape_452.graphics.f("#FFD56C").s().p("EgtSARpIAAgbUAvVgBbArQghbMAAAAjRg");
	this.shape_452.setTransform(290,116.1);

	this.shape_453 = new cjs.Shape();
	this.shape_453.graphics.f("#FFD56C").s().p("EgtRARnIAAgbUAvSgBdArRghVMAAAAjNg");
	this.shape_453.setTransform(289.9,116.3);

	this.shape_454 = new cjs.Shape();
	this.shape_454.graphics.f("#FFD56C").s().p("EgtQARkIAAgaUAvQgBeArRghQMAAAAjIg");
	this.shape_454.setTransform(289.8,116.5);

	this.shape_455 = new cjs.Shape();
	this.shape_455.graphics.f("#FFD56C").s().p("EgtPARiIAAgaUAvNgBgArSghJMAAAAjDg");
	this.shape_455.setTransform(289.7,116.7);

	this.shape_456 = new cjs.Shape();
	this.shape_456.graphics.f("#FFD56C").s().p("EgtOARgIAAgaUAvKgBiArTghDMAAAAi/g");
	this.shape_456.setTransform(289.6,116.9);

	this.shape_457 = new cjs.Shape();
	this.shape_457.graphics.f("#FFD56C").s().p("EgtNAReIAAgbUAvIgBjArTgg9MAAAAi7g");
	this.shape_457.setTransform(289.5,117.2);

	this.shape_458 = new cjs.Shape();
	this.shape_458.graphics.f("#FFD56C").s().p("EgtMARcIAAgbUAvFgBjArUgg5MAAAAi3g");
	this.shape_458.setTransform(289.4,117.4);

	this.shape_459 = new cjs.Shape();
	this.shape_459.graphics.f("#FFD56C").s().p("EgtLARaIAAgbUAvCgBlArVggzMAAAAizg");
	this.shape_459.setTransform(289.3,117.6);

	this.shape_460 = new cjs.Shape();
	this.shape_460.graphics.f("#FFD56C").s().p("EgtKARYIAAgbUAu/gBnArWggtMAAAAivg");
	this.shape_460.setTransform(289.2,117.8);

	this.shape_461 = new cjs.Shape();
	this.shape_461.graphics.f("#FFD56C").s().p("EgtJARVIAAgaUAu9gBoArWggnMAAAAipg");
	this.shape_461.setTransform(289.1,118);

	this.shape_462 = new cjs.Shape();
	this.shape_462.graphics.f("#FFD56C").s().p("EgtIARTIAAgaUAu6gBqArXgghMAAAAilg");
	this.shape_462.setTransform(289,118.2);

	this.shape_463 = new cjs.Shape();
	this.shape_463.graphics.f("#FFD56C").s().p("EgtHARRIAAgaUAu2gBsArZggbMAAAAihg");
	this.shape_463.setTransform(288.9,118.4);

	this.shape_464 = new cjs.Shape();
	this.shape_464.graphics.f("#FFD56C").s().p("EgtGARPIAAgbUAu0gBsArZggWMAAAAidg");
	this.shape_464.setTransform(288.8,118.7);

	this.shape_465 = new cjs.Shape();
	this.shape_465.graphics.f("#FFD56C").s().p("EgtFARNIAAgbUAuxgBtAraggRMAAAAiZg");
	this.shape_465.setTransform(288.7,118.9);

	this.shape_466 = new cjs.Shape();
	this.shape_466.graphics.f("#FFD56C").s().p("EgtEARLIAAgbUAuugBvArbggLMAAAAiVg");
	this.shape_466.setTransform(288.6,119.1);

	this.shape_467 = new cjs.Shape();
	this.shape_467.graphics.f("#FFD56C").s().p("EgtDARJIAAgbUAurgBxArcggFMAAAAiRg");
	this.shape_467.setTransform(288.5,119.3);

	this.shape_468 = new cjs.Shape();
	this.shape_468.graphics.f("#FFD56C").s().p("EgtCARGIAAgaUAupgByArcggAMAAAAiMg");
	this.shape_468.setTransform(288.4,119.5);

	this.shape_469 = new cjs.Shape();
	this.shape_469.graphics.f("#FFD56C").s().p("EgtBAREIAAgaUAumgB0Ardgf5MAAAAiHg");
	this.shape_469.setTransform(288.3,119.7);

	this.shape_470 = new cjs.Shape();
	this.shape_470.graphics.f("#FFD56C").s().p("EgtAARCIAAgaUAujgB2AregfzMAAAAiDg");
	this.shape_470.setTransform(288.2,119.9);

	this.shape_471 = new cjs.Shape();
	this.shape_471.graphics.f("#FFD56C").s().p("Egs/ARAIAAgbUAuhgB2AregfuMAAAAh/g");
	this.shape_471.setTransform(288.1,120.2);

	this.shape_472 = new cjs.Shape();
	this.shape_472.graphics.f("#FFD56C").s().p("Egs+AQ+IAAgbUAuegB4ArfgfoMAAAAh7g");
	this.shape_472.setTransform(288,120.4);

	this.shape_473 = new cjs.Shape();
	this.shape_473.graphics.f("#FFD56C").s().p("Egs9AQ8IAAgbUAubgB5ArggfjMAAAAh3g");
	this.shape_473.setTransform(287.9,120.6);

	this.shape_474 = new cjs.Shape();
	this.shape_474.graphics.f("#FFD56C").s().p("Egs8AQ6IAAgbUAuYgB7ArhgfdMAAAAhzg");
	this.shape_474.setTransform(287.8,120.8);

	this.shape_475 = new cjs.Shape();
	this.shape_475.graphics.f("#FFD56C").s().p("Egs7AQ3IAAgaUAuWgB8ArhgfYMAAAAhug");
	this.shape_475.setTransform(287.7,121);

	this.shape_476 = new cjs.Shape();
	this.shape_476.graphics.f("#FFD56C").s().p("Egs6AQ1IAAgaUAuTgB+ArigfRMAAAAhpg");
	this.shape_476.setTransform(287.6,121.2);

	this.shape_477 = new cjs.Shape();
	this.shape_477.graphics.f("#FFD56C").s().p("Egs5AQzIAAgaUAuQgB/ArjgfMMAAAAhlg");
	this.shape_477.setTransform(287.5,121.4);

	this.shape_478 = new cjs.Shape();
	this.shape_478.graphics.f("#FFD56C").s().p("Egs4AQxIAAgbUAuNgCAArkgfGMAAAAhhg");
	this.shape_478.setTransform(287.4,121.7);

	this.shape_479 = new cjs.Shape();
	this.shape_479.graphics.f("#FFD56C").s().p("Egs3AQvIAAgbUAuLgCCArkgfAMAAAAhdg");
	this.shape_479.setTransform(287.3,121.9);

	this.shape_480 = new cjs.Shape();
	this.shape_480.graphics.f("#FFD56C").s().p("Egs2AQtIAAgbUAuHgCDArmge7MAAAAhZg");
	this.shape_480.setTransform(287.2,122.1);

	this.shape_481 = new cjs.Shape();
	this.shape_481.graphics.f("#FFD56C").s().p("Egs1AQrIAAgbUAuFgCFArmge1MAAAAhVg");
	this.shape_481.setTransform(287.1,122.3);

	this.shape_482 = new cjs.Shape();
	this.shape_482.graphics.f("#FFD56C").s().p("Egs0AQoIAAgaUAuCgCGArngevMAAAAhPg");
	this.shape_482.setTransform(287,122.5);

	this.shape_483 = new cjs.Shape();
	this.shape_483.graphics.f("#FFD56C").s().p("EgszAQmIAAgaUAuAgCIArngepMAAAAhLg");
	this.shape_483.setTransform(286.9,122.7);

	this.shape_484 = new cjs.Shape();
	this.shape_484.graphics.f("#FFD56C").s().p("EgsyAQkIAAgaUAt8gCJArpgekMAAAAhHg");
	this.shape_484.setTransform(286.8,122.9);

	this.shape_485 = new cjs.Shape();
	this.shape_485.graphics.f("#FFD56C").s().p("EgsxAQiIAAgbUAt6gCKArpgeeMAAAAhDg");
	this.shape_485.setTransform(286.7,123.2);

	this.shape_486 = new cjs.Shape();
	this.shape_486.graphics.f("#FFD56C").s().p("EgswAQgIAAgbUAt3gCLArqgeZMAAAAg/g");
	this.shape_486.setTransform(286.6,123.4);

	this.shape_487 = new cjs.Shape();
	this.shape_487.graphics.f("#FFD56C").s().p("EgsvAQeIAAgbUAt0gCNArrgeTMAAAAg7g");
	this.shape_487.setTransform(286.5,123.6);

	this.shape_488 = new cjs.Shape();
	this.shape_488.graphics.f("#FFD56C").s().p("EgsuAQcIAAgbUAtxgCOArsgeOMAAAAg3g");
	this.shape_488.setTransform(286.4,123.8);

	this.shape_489 = new cjs.Shape();
	this.shape_489.graphics.f("#FFD56C").s().p("EgstAQZIAAgaUAtvgCQArsgeIMAAAAgyg");
	this.shape_489.setTransform(286.3,124);

	this.shape_490 = new cjs.Shape();
	this.shape_490.graphics.f("#FFD56C").s().p("EgssAQXIAAgaUAtsgCSArtgeBMAAAAgtg");
	this.shape_490.setTransform(286.2,124.2);

	this.shape_491 = new cjs.Shape();
	this.shape_491.graphics.f("#FFD56C").s().p("EgsrAQVIAAgaUAtpgCTArugd8MAAAAgpg");
	this.shape_491.setTransform(286.1,124.4);

	this.shape_492 = new cjs.Shape();
	this.shape_492.graphics.f("#FFD56C").s().p("EgsqAQTIAAgbUAtmgCUArvgd2MAAAAglg");
	this.shape_492.setTransform(286,124.7);

	this.shape_493 = new cjs.Shape();
	this.shape_493.graphics.f("#FFD56C").s().p("EgspAQRIAAgbUAtkgCWArvgdwMAAAAghg");
	this.shape_493.setTransform(285.9,124.9);

	this.shape_494 = new cjs.Shape();
	this.shape_494.graphics.f("#FFD56C").s().p("EgsoAQPIAAgbUAthgCXArwgdrMAAAAgdg");
	this.shape_494.setTransform(285.8,125.1);

	this.shape_495 = new cjs.Shape();
	this.shape_495.graphics.f("#FFD56C").s().p("EgsnAQNIAAgbUAtegCYArxgdmMAAAAgZg");
	this.shape_495.setTransform(285.7,125.3);

	this.shape_496 = new cjs.Shape();
	this.shape_496.graphics.f("#FFD56C").s().p("EgsmAQKIAAgaUAtcgCaArxgdgMAAAAgUg");
	this.shape_496.setTransform(285.6,125.5);

	this.shape_497 = new cjs.Shape();
	this.shape_497.graphics.f("#FFD56C").s().p("EgslAQIIAAgaUAtZgCcArygdZMAAAAgPg");
	this.shape_497.setTransform(285.5,125.7);

	this.shape_498 = new cjs.Shape();
	this.shape_498.graphics.f("#FFD56C").s().p("EgskAQGIAAgaUAtWgCdArzgdUMAAAAgLg");
	this.shape_498.setTransform(285.4,125.9);

	this.shape_499 = new cjs.Shape();
	this.shape_499.graphics.f("#FFD56C").s().p("EgsjAQEIAAgbUAtTgCeAr0gdOMAAAAgHg");
	this.shape_499.setTransform(285.3,126.2);

	this.shape_500 = new cjs.Shape();
	this.shape_500.graphics.f("#FFD56C").s().p("EgsiAQCIAAgbUAtRgCfAr0gdJMAAAAgDg");
	this.shape_500.setTransform(285.2,126.4);

	this.shape_501 = new cjs.Shape();
	this.shape_501.graphics.f("#FFD56C").s().p("EgshAQAIAAgbUAtOgChAr1gdDIAAf/g");
	this.shape_501.setTransform(285.1,126.6);

	this.shape_502 = new cjs.Shape();
	this.shape_502.graphics.f("#FFD56C").s().p("EgsgAP+IAAgbUAtLgCiAr2gc+IAAf7g");
	this.shape_502.setTransform(285,126.8);

	this.shape_503 = new cjs.Shape();
	this.shape_503.graphics.f("#FFD56C").s().p("EgsfAP7IAAgaUAtIgCkAr3gc3IAAf1g");
	this.shape_503.setTransform(284.9,127);

	this.shape_504 = new cjs.Shape();
	this.shape_504.graphics.f("#FFD56C").s().p("EgtrASdIAAgaUAwagA5Aq9gjmQhPTSBPRng");
	this.shape_504.setTransform(292.4,110.8);

	this.shape_505 = new cjs.Shape();
	this.shape_505.graphics.f("#FFD56C").s().p("EgtiASLIAAgaUAwCgBFArDgi2QicT2CcQfg");
	this.shape_505.setTransform(291.6,112.6);

	this.shape_506 = new cjs.Shape();
	this.shape_506.graphics.f("#FFD56C").s().p("EgtaAR5IAAgaUAvrgBQArKgiHQjrUbDrPWg");
	this.shape_506.setTransform(290.7,114.4);

	this.shape_507 = new cjs.Shape();
	this.shape_507.graphics.f("#FFD56C").s().p("EgtSARnIAAgaUAvUgBdArQghWQk4U/E4OOg");
	this.shape_507.setTransform(289.9,116.2);

	this.shape_508 = new cjs.Shape();
	this.shape_508.graphics.f("#FFD56C").s().p("EgtJARVIAAgaUAu9gBoArWggnQmHVkGHNFg");
	this.shape_508.setTransform(289.1,118);

	this.shape_509 = new cjs.Shape();
	this.shape_509.graphics.f("#FFD56C").s().p("EgtBARDIAAgaUAulgB1Ardgf2QnUWJHUL8g");
	this.shape_509.setTransform(288.2,119.8);

	this.shape_510 = new cjs.Shape();
	this.shape_510.graphics.f("#FFD56C").s().p("Egs4AQxIAAgaUAuOgCBArjgfGQojWtIjK0g");
	this.shape_510.setTransform(287.4,121.6);

	this.shape_511 = new cjs.Shape();
	this.shape_511.graphics.f("#FFD56C").s().p("EgswAQfIAAgaUAt3gCMArqgeXQpxXSJxJrg");
	this.shape_511.setTransform(286.5,123.4);

	this.shape_512 = new cjs.Shape();
	this.shape_512.graphics.f("#FFD56C").s().p("EgsoAQNIAAgaUAtggCYArxgdnQrAX2LAIjg");
	this.shape_512.setTransform(285.7,125.2);

	this.shape_513 = new cjs.Shape();
	this.shape_513.graphics.f("#FFD56C").s().p("EgsfAP7IAAgaUAtIgCkAr3gc3QsOYbMOHag");
	this.shape_513.setTransform(284.9,127);

	this.shape_514 = new cjs.Shape();
	this.shape_514.graphics.f("#FFD56C").s().p("EgsjAP8IAAgaUAtWgCvArxgcuQsHYeMGHZg");
	this.shape_514.setTransform(285.2,126.9);

	this.shape_515 = new cjs.Shape();
	this.shape_515.graphics.f("#FFD56C").s().p("EgstAP/IAAgbUAt+gDOArdgcUQrwYnLuHWg");
	this.shape_515.setTransform(286.3,126.7);

	this.shape_516 = new cjs.Shape();
	this.shape_516.graphics.f("#FFD56C").s().p("Egs/AQDIAAgbUAvDgEDAq8gbnQrLY3LFHOg");
	this.shape_516.setTransform(288.1,126.3);

	this.shape_517 = new cjs.Shape();
	this.shape_517.graphics.f("#FFD56C").s().p("EgtYAQJIAAgbUAwigFOAqPgaoQqXZNKNHEg");
	this.shape_517.setTransform(290.5,125.7);

	this.shape_518 = new cjs.Shape();
	this.shape_518.graphics.f("#FFD56C").s().p("Egt4AQQIAAgbUAycgGuApVgZWQpUZoJFG3g");
	this.shape_518.setTransform(293.7,125);

	this.shape_519 = new cjs.Shape();
	this.shape_519.graphics.f("#FFD56C").s().p("EgufAQZIAAgbUA0xgIjAoOgXzQoCaJHtGog");
	this.shape_519.setTransform(297.6,124.1);

	this.shape_520 = new cjs.Shape();
	this.shape_520.graphics.f("#FFD56C").s().p("EgvNAQkIAAgbUA3igKuAm5gV+QmiayGFGVg");
	this.shape_520.setTransform(302.2,123);

	this.shape_521 = new cjs.Shape();
	this.shape_521.graphics.f("#FFD56C").s().p("EgwCAQwIAAgbUA6tgNOAlYgT2QkybgENF/g");
	this.shape_521.setTransform(307.6,121.8);

	this.shape_522 = new cjs.Shape();
	this.shape_522.graphics.f("#FFD56C").s().p("Egw+AQ+IAAgbUA+UgQCAjpgReQizcUCEFng");
	this.shape_522.setTransform(313.6,120.4);

	this.shape_523 = new cjs.Shape();
	this.shape_523.graphics.f("#FFD56C").s().p("EgyCARNIAAgaUBCXgTLAhugO0QgndNgTFMg");
	this.shape_523.setTransform(320.3,118.8);

	this.shape_524 = new cjs.Shape();
	this.shape_524.graphics.f("#FFD56C").s().p("EgzjARfIAAgbUBG0gWrAflgL3QB2ePi8Eug");
	this.shape_524.setTransform(330,117.1);

	this.shape_525 = new cjs.Shape();
	this.shape_525.graphics.f("#FFD56C").s().p("Eg1dARxIAAgaUBLtgahAdQgImQEgfVl0EMg");
	this.shape_525.setTransform(342.2,115.2);

	this.shape_526 = new cjs.Shape();
	this.shape_526.graphics.f("#FFD56C").s().p("Eg3iASGIAAgbUBRAgerAaugFFUAHbAghgI9ADqg");
	this.shape_526.setTransform(355.5,113.2);

	this.shape_527 = new cjs.Shape();
	this.shape_527.graphics.f("#FFD56C").s().p("Eg5yAScIAAgaUBWvgjLAX+gBSUAKkAh0gMVADDg");
	this.shape_527.setTransform(369.9,110.9);

	this.shape_528 = new cjs.Shape();
	this.shape_528.graphics.f("#FFD56C").s().p("Eg8MAS6IAAgbUBc5goAAVCAC0UAN6AjNgP9ACag");
	this.shape_528.setTransform(385.4,108);

	this.shape_529 = new cjs.Shape();
	this.shape_529.graphics.f("#FFD56C").s().p("Eg+yATtIAAgbUBjegtKAR5AHLUARhAktgT1ABtg");
	this.shape_529.setTransform(401.9,102.9);

	this.shape_530 = new cjs.Shape();
	this.shape_530.graphics.f("#FFD56C").s().p("EhBiAUwIAAgaUBqfgyqAOiAL0UAVWAmSgX9AA+g");
	this.shape_530.setTransform(419.5,96.1);

	this.shape_531 = new cjs.Shape();
	this.shape_531.graphics.f("#FFD56C").s().p("EhEcAWAIAAgbUBx6g4fAK/AQwUAZaAn+gcWAAMg");
	this.shape_531.setTransform(438.2,88.2);

	this.shape_532 = new cjs.Shape();
	this.shape_532.graphics.f("rgba(255,213,108,0.925)").s().p("EhEWAUkIAAgaUBwTg0wAMkAPnQIzLaCJINQEsR0zOAIg");
	this.shape_532.setTransform(437.6,97.3);

	this.shape_533 = new cjs.Shape();
	this.shape_533.graphics.f("rgba(255,213,108,0.851)").s().p("EhESATMIgCAAIAAgaUBuxgxLAOFAOhQJwKmBkH8QDPQbyyAHg");
	this.shape_533.setTransform(437.3,106.1);

	this.shape_534 = new cjs.Shape();
	this.shape_534.graphics.f("rgba(255,213,108,0.784)").s().p("EhETAR4IgCAAIAAgaUBtSgtuAPjANdQKrJ0BAHsQB3PFyYAGg");
	this.shape_534.setTransform(437.4,114.5);

	this.shape_535 = new cjs.Shape();
	this.shape_535.graphics.f("rgba(255,213,108,0.718)").s().p("EhEZAQnIgBAAIAAgaUBr3gqcAQ8AMdQLkJFAdHbQAiNzx/AGg");
	this.shape_535.setTransform(438,122.6);

	this.shape_536 = new cjs.Shape();
	this.shape_536.graphics.f("rgba(255,213,108,0.651)").s().p("EhEiAPaIgCAAIAAgaUBqggnSASSALfQMaIXgDHMQgwMlxmAFg");
	this.shape_536.setTransform(438.9,130.3);

	this.shape_537 = new cjs.Shape();
	this.shape_537.graphics.f("rgba(255,213,108,0.592)").s().p("EhEtAORIgBgBIAAgaUBpOgkRATjAKkQNNHrgiG+Qh9LaxQAFg");
	this.shape_537.setTransform(439.9,137.7);

	this.shape_538 = new cjs.Shape();
	this.shape_538.graphics.f("rgba(255,213,108,0.533)").s().p("EhE4ANLIgBgBIAAgaUBn/ghbAUxAJsQN+HDhAGwQjHKSw6AFg");
	this.shape_538.setTransform(441,144.7);

	this.shape_539 = new cjs.Shape();
	this.shape_539.graphics.f("rgba(255,213,108,0.478)").s().p("EhFCAMIIgBgBIAAgZUBm0geuAV6AI3QOtGbhcGjQkNJPwlAEg");
	this.shape_539.setTransform(442.1,151.3);

	this.shape_540 = new cjs.Shape();
	this.shape_540.graphics.f("rgba(255,213,108,0.427)").s().p("EhFNALKIgBgBIAAgaUBlugcJAXAAIFQPZF1h3GXQlPIPwSAEg");
	this.shape_540.setTransform(443.1,157.6);

	this.shape_541 = new cjs.Shape();
	this.shape_541.graphics.f("rgba(255,213,108,0.38)").s().p("EhFXAKOIgBgBIAAgZUBkrgZvAYCAHVQQCFTiQGLQmNHTv/ADg");
	this.shape_541.setTransform(444.1,163.5);

	this.shape_542 = new cjs.Shape();
	this.shape_542.graphics.f("rgba(255,213,108,0.333)").s().p("EhFhAJXIAAgCIAAgZUBjsgXdAY/AGpQQqExioGBQnIGavuADg");
	this.shape_542.setTransform(445.1,169.1);

	this.shape_543 = new cjs.Shape();
	this.shape_543.graphics.f("rgba(255,213,108,0.29)").s().p("EhFqAIjIgBgCIAAgZUBiygVVAZ5AF/QROETi+F2Qn+FlveADg");
	this.shape_543.setTransform(446,174.3);

	this.shape_544 = new cjs.Shape();
	this.shape_544.graphics.f("rgba(255,213,108,0.251)").s().p("EhFzAHyIgBgBIAAgZUBh8gTXAauAFYQRwD3jSFsQoyE0vPACg");
	this.shape_544.setTransform(446.9,179.1);

	this.shape_545 = new cjs.Shape();
	this.shape_545.graphics.f("rgba(255,213,108,0.212)").s().p("EhF7AHGIgBgCIAAgZUBhKgRiAbfAE1QSQDcjmFjQpgEHvBACg");
	this.shape_545.setTransform(447.7,183.6);

	this.shape_546 = new cjs.Shape();
	this.shape_546.graphics.f("rgba(255,213,108,0.18)").s().p("EhGDAGcIAAgBIAAgZUBgbgP2AcNAETQSsDEj3FbQqMDdu0ABg");
	this.shape_546.setTransform(448.5,187.7);

	this.shape_547 = new cjs.Shape();
	this.shape_547.graphics.f("rgba(255,213,108,0.149)").s().p("EhGKAF3IgBgCIAAgZUBfxgOTAc3AD1QTHCtkHFUQq0C2uoACg");
	this.shape_547.setTransform(449.2,191.5);

	this.shape_548 = new cjs.Shape();
	this.shape_548.graphics.f("rgba(255,213,108,0.122)").s().p("EhGRAFVIAAgbUBfLgM6AdcADaQTfCZkVFNQrYCUueABg");
	this.shape_548.setTransform(449.8,194.9);

	this.shape_549 = new cjs.Shape();
	this.shape_549.graphics.f("rgba(255,213,108,0.094)").s().p("EhGXAE2IAAgaUBeogLrAd/ADCQT0CHkjFHQr3B0uUABg");
	this.shape_549.setTransform(450.4,197.9);

	this.shape_550 = new cjs.Shape();
	this.shape_550.graphics.f("rgba(255,213,108,0.071)").s().p("EhGcAEbIAAgaUBeKgKlAecACsQUHB3kuFCQsUBauLAAg");
	this.shape_550.setTransform(450.9,200.6);

	this.shape_551 = new cjs.Shape();
	this.shape_551.graphics.f("rgba(255,213,108,0.055)").s().p("EhGgAEEIAAgaUBdvgJoAe2ACaQUYBpk4E9QstBCuEAAg");
	this.shape_551.setTransform(451.4,202.9);

	this.shape_552 = new cjs.Shape();
	this.shape_552.graphics.f("rgba(255,213,108,0.035)").s().p("EhGkADwIAAgaUBdZgI0AfMACJQUlBelAE5QtBAut/AAg");
	this.shape_552.setTransform(451.8,204.9);

	this.shape_553 = new cjs.Shape();
	this.shape_553.graphics.f("rgba(255,213,108,0.024)").s().p("EhGnADgIAAgaUBdHgIKAfeAB9QUwBUlHE2QtSAdt5AAg");
	this.shape_553.setTransform(452.1,206.5);

	this.shape_554 = new cjs.Shape();
	this.shape_554.graphics.f("rgba(255,213,108,0.012)").s().p("EhGqADUIAAgbUBc5gHpAfsABzQU5BNlMEzQtfARt2AAg");
	this.shape_554.setTransform(452.3,207.8);

	this.shape_555 = new cjs.Shape();
	this.shape_555.graphics.f("rgba(255,213,108,0.008)").s().p("EhGrADLIAAgbUBcvgHSAf1ABsQVABHlQEyQtpAItzAAg");
	this.shape_555.setTransform(452.5,208.7);

	this.shape_556 = new cjs.Shape();
	this.shape_556.graphics.f("rgba(255,213,108,0)").s().p("EhGsADGIAAgbUBcpgHDAf7ABnQVEBElTExQtuACtxAAg");
	this.shape_556.setTransform(452.6,209.2);

	this.shape_557 = new cjs.Shape();
	this.shape_557.graphics.f("rgba(255,213,108,0)").s().p("EhGtADEIAAgbUCZsgLngNWAMCg");
	this.shape_557.setTransform(452.6,209.4);

	this.shape_279.mask = this.shape_280.mask = this.shape_281.mask = this.shape_282.mask = this.shape_283.mask = this.shape_284.mask = this.shape_285.mask = this.shape_286.mask = this.shape_287.mask = this.shape_288.mask = this.shape_289.mask = this.shape_290.mask = this.shape_291.mask = this.shape_292.mask = this.shape_293.mask = this.shape_294.mask = this.shape_295.mask = this.shape_296.mask = this.shape_297.mask = this.shape_298.mask = this.shape_299.mask = this.shape_300.mask = this.shape_301.mask = this.shape_302.mask = this.shape_303.mask = this.shape_304.mask = this.shape_305.mask = this.shape_306.mask = this.shape_307.mask = this.shape_308.mask = this.shape_309.mask = this.shape_310.mask = this.shape_311.mask = this.shape_312.mask = this.shape_313.mask = this.shape_314.mask = this.shape_315.mask = this.shape_316.mask = this.shape_317.mask = this.shape_318.mask = this.shape_319.mask = this.shape_320.mask = this.shape_321.mask = this.shape_322.mask = this.shape_323.mask = this.shape_324.mask = this.shape_325.mask = this.shape_326.mask = this.shape_327.mask = this.shape_328.mask = this.shape_329.mask = this.shape_330.mask = this.shape_331.mask = this.shape_332.mask = this.shape_333.mask = this.shape_334.mask = this.shape_335.mask = this.shape_336.mask = this.shape_337.mask = this.shape_338.mask = this.shape_339.mask = this.shape_340.mask = this.shape_341.mask = this.shape_342.mask = this.shape_343.mask = this.shape_344.mask = this.shape_345.mask = this.shape_346.mask = this.shape_347.mask = this.shape_348.mask = this.shape_349.mask = this.shape_350.mask = this.shape_351.mask = this.shape_352.mask = this.shape_353.mask = this.shape_354.mask = this.shape_355.mask = this.shape_356.mask = this.shape_357.mask = this.shape_358.mask = this.shape_359.mask = this.shape_360.mask = this.shape_361.mask = this.shape_362.mask = this.shape_363.mask = this.shape_364.mask = this.shape_365.mask = this.shape_366.mask = this.shape_367.mask = this.shape_368.mask = this.shape_369.mask = this.shape_370.mask = this.shape_371.mask = this.shape_372.mask = this.shape_373.mask = this.shape_374.mask = this.shape_375.mask = this.shape_376.mask = this.shape_377.mask = this.shape_378.mask = this.shape_379.mask = this.shape_380.mask = this.shape_381.mask = this.shape_382.mask = this.shape_383.mask = this.shape_384.mask = this.shape_385.mask = this.shape_386.mask = this.shape_387.mask = this.shape_388.mask = this.shape_389.mask = this.shape_390.mask = this.shape_391.mask = this.shape_392.mask = this.shape_393.mask = this.shape_394.mask = this.shape_395.mask = this.shape_396.mask = this.shape_397.mask = this.shape_398.mask = this.shape_399.mask = this.shape_400.mask = this.shape_401.mask = this.shape_402.mask = this.shape_403.mask = this.shape_404.mask = this.shape_405.mask = this.shape_406.mask = this.shape_407.mask = this.shape_408.mask = this.shape_409.mask = this.shape_410.mask = this.shape_411.mask = this.shape_412.mask = this.shape_413.mask = this.shape_414.mask = this.shape_415.mask = this.shape_416.mask = this.shape_417.mask = this.shape_418.mask = this.shape_419.mask = this.shape_420.mask = this.shape_421.mask = this.shape_422.mask = this.shape_423.mask = this.shape_424.mask = this.shape_425.mask = this.shape_426.mask = this.shape_427.mask = this.shape_428.mask = this.shape_429.mask = this.shape_430.mask = this.shape_431.mask = this.shape_432.mask = this.shape_433.mask = this.shape_434.mask = this.shape_435.mask = this.shape_436.mask = this.shape_437.mask = this.shape_438.mask = this.shape_439.mask = this.shape_440.mask = this.shape_441.mask = this.shape_442.mask = this.shape_443.mask = this.shape_444.mask = this.shape_445.mask = this.shape_446.mask = this.shape_447.mask = this.shape_448.mask = this.shape_449.mask = this.shape_450.mask = this.shape_451.mask = this.shape_452.mask = this.shape_453.mask = this.shape_454.mask = this.shape_455.mask = this.shape_456.mask = this.shape_457.mask = this.shape_458.mask = this.shape_459.mask = this.shape_460.mask = this.shape_461.mask = this.shape_462.mask = this.shape_463.mask = this.shape_464.mask = this.shape_465.mask = this.shape_466.mask = this.shape_467.mask = this.shape_468.mask = this.shape_469.mask = this.shape_470.mask = this.shape_471.mask = this.shape_472.mask = this.shape_473.mask = this.shape_474.mask = this.shape_475.mask = this.shape_476.mask = this.shape_477.mask = this.shape_478.mask = this.shape_479.mask = this.shape_480.mask = this.shape_481.mask = this.shape_482.mask = this.shape_483.mask = this.shape_484.mask = this.shape_485.mask = this.shape_486.mask = this.shape_487.mask = this.shape_488.mask = this.shape_489.mask = this.shape_490.mask = this.shape_491.mask = this.shape_492.mask = this.shape_493.mask = this.shape_494.mask = this.shape_495.mask = this.shape_496.mask = this.shape_497.mask = this.shape_498.mask = this.shape_499.mask = this.shape_500.mask = this.shape_501.mask = this.shape_502.mask = this.shape_503.mask = this.shape_504.mask = this.shape_505.mask = this.shape_506.mask = this.shape_507.mask = this.shape_508.mask = this.shape_509.mask = this.shape_510.mask = this.shape_511.mask = this.shape_512.mask = this.shape_513.mask = this.shape_514.mask = this.shape_515.mask = this.shape_516.mask = this.shape_517.mask = this.shape_518.mask = this.shape_519.mask = this.shape_520.mask = this.shape_521.mask = this.shape_522.mask = this.shape_523.mask = this.shape_524.mask = this.shape_525.mask = this.shape_526.mask = this.shape_527.mask = this.shape_528.mask = this.shape_529.mask = this.shape_530.mask = this.shape_531.mask = this.shape_532.mask = this.shape_533.mask = this.shape_534.mask = this.shape_535.mask = this.shape_536.mask = this.shape_537.mask = this.shape_538.mask = this.shape_539.mask = this.shape_540.mask = this.shape_541.mask = this.shape_542.mask = this.shape_543.mask = this.shape_544.mask = this.shape_545.mask = this.shape_546.mask = this.shape_547.mask = this.shape_548.mask = this.shape_549.mask = this.shape_550.mask = this.shape_551.mask = this.shape_552.mask = this.shape_553.mask = this.shape_554.mask = this.shape_555.mask = this.shape_556.mask = this.shape_557.mask = mask;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_279}]}).to({state:[{t:this.shape_279}]},1).to({state:[{t:this.shape_280}]},1).to({state:[{t:this.shape_281}]},1).to({state:[{t:this.shape_282}]},1).to({state:[{t:this.shape_283}]},1).to({state:[{t:this.shape_284}]},1).to({state:[{t:this.shape_285}]},1).to({state:[{t:this.shape_286}]},1).to({state:[{t:this.shape_287}]},1).to({state:[{t:this.shape_288}]},1).to({state:[{t:this.shape_289}]},1).to({state:[{t:this.shape_290}]},1).to({state:[{t:this.shape_291}]},1).to({state:[{t:this.shape_292}]},1).to({state:[{t:this.shape_293}]},1).to({state:[{t:this.shape_294}]},1).to({state:[{t:this.shape_295}]},1).to({state:[{t:this.shape_296}]},1).to({state:[{t:this.shape_297}]},1).to({state:[{t:this.shape_298}]},1).to({state:[{t:this.shape_299}]},1).to({state:[{t:this.shape_300}]},1).to({state:[{t:this.shape_301}]},1).to({state:[{t:this.shape_302}]},1).to({state:[{t:this.shape_303}]},1).to({state:[{t:this.shape_304}]},1).to({state:[{t:this.shape_305}]},1).to({state:[{t:this.shape_306}]},1).to({state:[{t:this.shape_307}]},1).to({state:[{t:this.shape_308}]},1).to({state:[{t:this.shape_309}]},1).to({state:[{t:this.shape_310}]},1).to({state:[{t:this.shape_311}]},1).to({state:[{t:this.shape_312}]},1).to({state:[{t:this.shape_313}]},1).to({state:[{t:this.shape_314}]},1).to({state:[{t:this.shape_315}]},1).to({state:[{t:this.shape_316}]},1).to({state:[{t:this.shape_317}]},1).to({state:[{t:this.shape_318}]},1).to({state:[{t:this.shape_319}]},1).to({state:[{t:this.shape_320}]},1).to({state:[{t:this.shape_321}]},1).to({state:[{t:this.shape_322}]},1).to({state:[{t:this.shape_323}]},1).to({state:[{t:this.shape_324}]},1).to({state:[{t:this.shape_325}]},1).to({state:[{t:this.shape_326}]},1).to({state:[{t:this.shape_327}]},1).to({state:[{t:this.shape_328}]},1).to({state:[{t:this.shape_329}]},1).to({state:[{t:this.shape_330}]},1).to({state:[{t:this.shape_331}]},1).to({state:[{t:this.shape_332}]},1).to({state:[{t:this.shape_333}]},1).to({state:[{t:this.shape_334}]},1).to({state:[{t:this.shape_335}]},1).to({state:[{t:this.shape_336}]},1).to({state:[{t:this.shape_337}]},1).to({state:[{t:this.shape_338}]},1).to({state:[{t:this.shape_339}]},1).to({state:[{t:this.shape_340}]},1).to({state:[{t:this.shape_341}]},1).to({state:[{t:this.shape_342}]},1).to({state:[{t:this.shape_343}]},1).to({state:[{t:this.shape_344}]},1).to({state:[{t:this.shape_345}]},1).to({state:[{t:this.shape_346}]},1).to({state:[{t:this.shape_347}]},1).to({state:[{t:this.shape_348}]},1).to({state:[{t:this.shape_349}]},1).to({state:[{t:this.shape_350}]},1).to({state:[{t:this.shape_351}]},1).to({state:[{t:this.shape_352}]},1).to({state:[{t:this.shape_353}]},1).to({state:[{t:this.shape_354}]},1).to({state:[{t:this.shape_355}]},1).to({state:[{t:this.shape_356}]},1).to({state:[{t:this.shape_357}]},1).to({state:[{t:this.shape_358}]},1).to({state:[{t:this.shape_359}]},1).to({state:[{t:this.shape_360}]},1).to({state:[{t:this.shape_361}]},1).to({state:[{t:this.shape_362}]},1).to({state:[{t:this.shape_363}]},1).to({state:[{t:this.shape_364}]},1).to({state:[{t:this.shape_365}]},1).to({state:[{t:this.shape_366}]},1).to({state:[{t:this.shape_367}]},1).to({state:[{t:this.shape_368}]},1).to({state:[{t:this.shape_369}]},1).to({state:[{t:this.shape_370}]},1).to({state:[{t:this.shape_371}]},1).to({state:[{t:this.shape_372}]},1).to({state:[{t:this.shape_373}]},1).to({state:[{t:this.shape_374}]},1).to({state:[{t:this.shape_375}]},1).to({state:[{t:this.shape_376}]},1).to({state:[{t:this.shape_377}]},1).to({state:[{t:this.shape_378}]},1).to({state:[{t:this.shape_379}]},1).to({state:[{t:this.shape_380}]},1).to({state:[{t:this.shape_381}]},1).to({state:[{t:this.shape_382}]},1).to({state:[{t:this.shape_383}]},1).to({state:[{t:this.shape_384}]},1).to({state:[{t:this.shape_385}]},1).to({state:[{t:this.shape_386}]},1).to({state:[{t:this.shape_387}]},1).to({state:[{t:this.shape_388}]},1).to({state:[{t:this.shape_389}]},1).to({state:[{t:this.shape_390}]},1).to({state:[{t:this.shape_391}]},1).to({state:[{t:this.shape_392}]},1).to({state:[{t:this.shape_393}]},1).to({state:[{t:this.shape_394}]},1).to({state:[{t:this.shape_395}]},1).to({state:[{t:this.shape_396}]},1).to({state:[{t:this.shape_397}]},1).to({state:[{t:this.shape_398}]},1).to({state:[{t:this.shape_399}]},1).to({state:[{t:this.shape_400}]},1).to({state:[{t:this.shape_401}]},1).to({state:[{t:this.shape_402}]},1).to({state:[{t:this.shape_403}]},1).to({state:[{t:this.shape_404}]},1).to({state:[{t:this.shape_405}]},1).to({state:[{t:this.shape_406}]},1).to({state:[{t:this.shape_407}]},1).to({state:[{t:this.shape_408}]},1).to({state:[{t:this.shape_409}]},1).to({state:[{t:this.shape_410}]},1).to({state:[{t:this.shape_411}]},1).to({state:[{t:this.shape_412}]},1).to({state:[{t:this.shape_413}]},1).to({state:[{t:this.shape_414}]},1).to({state:[{t:this.shape_415}]},1).to({state:[{t:this.shape_416}]},1).to({state:[{t:this.shape_417}]},1).to({state:[{t:this.shape_418}]},1).to({state:[{t:this.shape_419}]},1).to({state:[{t:this.shape_420}]},1).to({state:[{t:this.shape_421}]},1).to({state:[{t:this.shape_422}]},1).to({state:[{t:this.shape_423}]},1).to({state:[{t:this.shape_424}]},1).to({state:[{t:this.shape_425}]},1).to({state:[{t:this.shape_426}]},1).to({state:[{t:this.shape_427}]},1).to({state:[{t:this.shape_428}]},1).to({state:[{t:this.shape_429}]},1).to({state:[{t:this.shape_430}]},1).to({state:[{t:this.shape_431}]},1).to({state:[{t:this.shape_432}]},1).to({state:[{t:this.shape_433}]},1).to({state:[{t:this.shape_434}]},1).to({state:[{t:this.shape_435}]},1).to({state:[{t:this.shape_436}]},1).to({state:[{t:this.shape_437}]},1).to({state:[{t:this.shape_438}]},1).to({state:[{t:this.shape_439}]},1).to({state:[{t:this.shape_440}]},1).to({state:[{t:this.shape_441}]},1).to({state:[{t:this.shape_442}]},1).to({state:[{t:this.shape_443}]},1).to({state:[{t:this.shape_444}]},1).to({state:[{t:this.shape_445}]},1).to({state:[{t:this.shape_446}]},1).to({state:[{t:this.shape_447}]},1).to({state:[{t:this.shape_448}]},1).to({state:[{t:this.shape_449}]},1).to({state:[{t:this.shape_450}]},1).to({state:[{t:this.shape_451}]},1).to({state:[{t:this.shape_452}]},1).to({state:[{t:this.shape_453}]},1).to({state:[{t:this.shape_454}]},1).to({state:[{t:this.shape_455}]},1).to({state:[{t:this.shape_456}]},1).to({state:[{t:this.shape_457}]},1).to({state:[{t:this.shape_458}]},1).to({state:[{t:this.shape_459}]},1).to({state:[{t:this.shape_460}]},1).to({state:[{t:this.shape_461}]},1).to({state:[{t:this.shape_462}]},1).to({state:[{t:this.shape_463}]},1).to({state:[{t:this.shape_464}]},1).to({state:[{t:this.shape_465}]},1).to({state:[{t:this.shape_466}]},1).to({state:[{t:this.shape_467}]},1).to({state:[{t:this.shape_468}]},1).to({state:[{t:this.shape_469}]},1).to({state:[{t:this.shape_470}]},1).to({state:[{t:this.shape_471}]},1).to({state:[{t:this.shape_472}]},1).to({state:[{t:this.shape_473}]},1).to({state:[{t:this.shape_474}]},1).to({state:[{t:this.shape_475}]},1).to({state:[{t:this.shape_476}]},1).to({state:[{t:this.shape_477}]},1).to({state:[{t:this.shape_478}]},1).to({state:[{t:this.shape_479}]},1).to({state:[{t:this.shape_480}]},1).to({state:[{t:this.shape_481}]},1).to({state:[{t:this.shape_482}]},1).to({state:[{t:this.shape_483}]},1).to({state:[{t:this.shape_484}]},1).to({state:[{t:this.shape_485}]},1).to({state:[{t:this.shape_486}]},1).to({state:[{t:this.shape_487}]},1).to({state:[{t:this.shape_488}]},1).to({state:[{t:this.shape_489}]},1).to({state:[{t:this.shape_490}]},1).to({state:[{t:this.shape_491}]},1).to({state:[{t:this.shape_492}]},1).to({state:[{t:this.shape_493}]},1).to({state:[{t:this.shape_494}]},1).to({state:[{t:this.shape_495}]},1).to({state:[{t:this.shape_496}]},1).to({state:[{t:this.shape_497}]},1).to({state:[{t:this.shape_498}]},1).to({state:[{t:this.shape_499}]},1).to({state:[{t:this.shape_500}]},1).to({state:[{t:this.shape_501}]},1).to({state:[{t:this.shape_502}]},1).to({state:[{t:this.shape_503}]},1).to({state:[{t:this.shape_502}]},1).to({state:[{t:this.shape_501}]},1).to({state:[{t:this.shape_500}]},1).to({state:[{t:this.shape_499}]},1).to({state:[{t:this.shape_498}]},1).to({state:[{t:this.shape_497}]},1).to({state:[{t:this.shape_496}]},1).to({state:[{t:this.shape_495}]},1).to({state:[{t:this.shape_494}]},1).to({state:[{t:this.shape_493}]},1).to({state:[{t:this.shape_492}]},1).to({state:[{t:this.shape_491}]},1).to({state:[{t:this.shape_490}]},1).to({state:[{t:this.shape_489}]},1).to({state:[{t:this.shape_488}]},1).to({state:[{t:this.shape_487}]},1).to({state:[{t:this.shape_486}]},1).to({state:[{t:this.shape_485}]},1).to({state:[{t:this.shape_484}]},1).to({state:[{t:this.shape_483}]},1).to({state:[{t:this.shape_482}]},1).to({state:[{t:this.shape_481}]},1).to({state:[{t:this.shape_480}]},1).to({state:[{t:this.shape_479}]},1).to({state:[{t:this.shape_478}]},1).to({state:[{t:this.shape_477}]},1).to({state:[{t:this.shape_476}]},1).to({state:[{t:this.shape_475}]},1).to({state:[{t:this.shape_474}]},1).to({state:[{t:this.shape_473}]},1).to({state:[{t:this.shape_472}]},1).to({state:[{t:this.shape_471}]},1).to({state:[{t:this.shape_470}]},1).to({state:[{t:this.shape_469}]},1).to({state:[{t:this.shape_468}]},1).to({state:[{t:this.shape_467}]},1).to({state:[{t:this.shape_466}]},1).to({state:[{t:this.shape_465}]},1).to({state:[{t:this.shape_464}]},1).to({state:[{t:this.shape_463}]},1).to({state:[{t:this.shape_462}]},1).to({state:[{t:this.shape_461}]},1).to({state:[{t:this.shape_460}]},1).to({state:[{t:this.shape_459}]},1).to({state:[{t:this.shape_458}]},1).to({state:[{t:this.shape_457}]},1).to({state:[{t:this.shape_456}]},1).to({state:[{t:this.shape_455}]},1).to({state:[{t:this.shape_454}]},1).to({state:[{t:this.shape_453}]},1).to({state:[{t:this.shape_452}]},1).to({state:[{t:this.shape_451}]},1).to({state:[{t:this.shape_450}]},1).to({state:[{t:this.shape_449}]},1).to({state:[{t:this.shape_448}]},1).to({state:[{t:this.shape_447}]},1).to({state:[{t:this.shape_446}]},1).to({state:[{t:this.shape_445}]},1).to({state:[{t:this.shape_444}]},1).to({state:[{t:this.shape_443}]},1).to({state:[{t:this.shape_442}]},1).to({state:[{t:this.shape_441}]},1).to({state:[{t:this.shape_440}]},1).to({state:[{t:this.shape_439}]},1).to({state:[{t:this.shape_438}]},1).to({state:[{t:this.shape_437}]},1).to({state:[{t:this.shape_436}]},1).to({state:[{t:this.shape_435}]},1).to({state:[{t:this.shape_434}]},1).to({state:[{t:this.shape_433}]},1).to({state:[{t:this.shape_432}]},1).to({state:[{t:this.shape_431}]},1).to({state:[{t:this.shape_430}]},1).to({state:[{t:this.shape_429}]},1).to({state:[{t:this.shape_428}]},1).to({state:[{t:this.shape_427}]},1).to({state:[{t:this.shape_426}]},1).to({state:[{t:this.shape_425}]},1).to({state:[{t:this.shape_424}]},1).to({state:[{t:this.shape_423}]},1).to({state:[{t:this.shape_422}]},1).to({state:[{t:this.shape_421}]},1).to({state:[{t:this.shape_420}]},1).to({state:[{t:this.shape_419}]},1).to({state:[{t:this.shape_504}]},1).to({state:[{t:this.shape_505}]},1).to({state:[{t:this.shape_506}]},1).to({state:[{t:this.shape_507}]},1).to({state:[{t:this.shape_508}]},1).to({state:[{t:this.shape_509}]},1).to({state:[{t:this.shape_510}]},1).to({state:[{t:this.shape_511}]},1).to({state:[{t:this.shape_512}]},1).to({state:[{t:this.shape_513}]},1).to({state:[{t:this.shape_514}]},1).to({state:[{t:this.shape_515}]},1).to({state:[{t:this.shape_516}]},1).to({state:[{t:this.shape_517}]},1).to({state:[{t:this.shape_518}]},1).to({state:[{t:this.shape_519}]},1).to({state:[{t:this.shape_520}]},1).to({state:[{t:this.shape_521}]},1).to({state:[{t:this.shape_522}]},1).to({state:[{t:this.shape_523}]},1).to({state:[{t:this.shape_524}]},1).to({state:[{t:this.shape_525}]},1).to({state:[{t:this.shape_526}]},1).to({state:[{t:this.shape_527}]},1).to({state:[{t:this.shape_528}]},1).to({state:[{t:this.shape_529}]},1).to({state:[{t:this.shape_530}]},1).to({state:[{t:this.shape_531}]},1).to({state:[{t:this.shape_532}]},1).to({state:[{t:this.shape_533}]},1).to({state:[{t:this.shape_534}]},1).to({state:[{t:this.shape_535}]},1).to({state:[{t:this.shape_536}]},1).to({state:[{t:this.shape_537}]},1).to({state:[{t:this.shape_538}]},1).to({state:[{t:this.shape_539}]},1).to({state:[{t:this.shape_540}]},1).to({state:[{t:this.shape_541}]},1).to({state:[{t:this.shape_542}]},1).to({state:[{t:this.shape_543}]},1).to({state:[{t:this.shape_544}]},1).to({state:[{t:this.shape_545}]},1).to({state:[{t:this.shape_546}]},1).to({state:[{t:this.shape_547}]},1).to({state:[{t:this.shape_548}]},1).to({state:[{t:this.shape_549}]},1).to({state:[{t:this.shape_550}]},1).to({state:[{t:this.shape_551}]},1).to({state:[{t:this.shape_552}]},1).to({state:[{t:this.shape_553}]},1).to({state:[{t:this.shape_554}]},1).to({state:[{t:this.shape_555}]},1).to({state:[{t:this.shape_556}]},1).to({state:[{t:this.shape_557}]},1).wait(1));

	// flameBlue
	this.flameBlue = new lib.flameBlue();
	this.flameBlue.setTransform(637.5,245.5,1,1,0,0,0,3.5,3.5);

	this.timeline.addTween(cjs.Tween.get(this.flameBlue).wait(1).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).wait(27));

	// flameBlue
	this.flameBlue_1 = new lib.flameBlue();
	this.flameBlue_1.setTransform(517.5,245.5,1,1,0,0,0,3.5,3.5);

	this.timeline.addTween(cjs.Tween.get(this.flameBlue_1).wait(1).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).to({scaleX:2.11,scaleY:2.11},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1},17,cjs.Ease.get(1)).wait(27));

	// flameBlue
	this.flameBlue_2 = new lib.flameBlue();
	this.flameBlue_2.setTransform(396.5,245.5,1,1,0,0,0,3.5,3.5);

	this.timeline.addTween(cjs.Tween.get(this.flameBlue_2).wait(1).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:396.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:396.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:396.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:396.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:396.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:396.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:396.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:396.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:396.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:396.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:396.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:396.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:396.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:396.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:396.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:396.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:396.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:396.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:396.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:396.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:396.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:396.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:396.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:396.5},17,cjs.Ease.get(1)).wait(27));

	// flameBlue
	this.flameBlue_3 = new lib.flameBlue();
	this.flameBlue_3.setTransform(277.5,245.5,1,1,0,0,0,3.5,3.5);

	this.timeline.addTween(cjs.Tween.get(this.flameBlue_3).wait(1).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:277.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:277.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:277.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:277.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:277.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:277.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:277.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:277.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:277.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:277.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:277.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:277.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:277.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:277.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:277.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:277.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:277.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:277.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:277.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:277.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:277.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:277.5},17,cjs.Ease.get(1)).to({regX:3.6,scaleX:2.11,scaleY:2.11,x:277.7},11,cjs.Ease.get(-0.4)).to({regX:3.5,scaleX:1,scaleY:1,x:277.5},17,cjs.Ease.get(1)).wait(27));

	// flameBlue
	this.flameBlue_4 = new lib.flameBlue();
	this.flameBlue_4.setTransform(155.5,245.5,1,1,0,0,0,3.5,3.5);

	this.timeline.addTween(cjs.Tween.get(this.flameBlue_4).wait(1).to({regX:3.6,regY:3.6,scaleX:2.13,scaleY:2.13,x:155.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:155.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.13,scaleY:2.13,x:155.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:155.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.13,scaleY:2.13,x:155.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:155.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.13,scaleY:2.13,x:155.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:155.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.13,scaleY:2.13,x:155.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:155.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.13,scaleY:2.13,x:155.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:155.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.13,scaleY:2.13,x:155.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:155.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.13,scaleY:2.13,x:155.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:155.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.13,scaleY:2.13,x:155.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:155.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.13,scaleY:2.13,x:155.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:155.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.13,scaleY:2.13,x:155.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:155.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.13,scaleY:2.13,x:155.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:155.5,y:245.5},17,cjs.Ease.get(1)).wait(27));

	// flameBlue
	this.flameBlue_5 = new lib.flameBlue();
	this.flameBlue_5.setTransform(35.5,245.5,1,1,0,0,0,3.5,3.5);

	this.timeline.addTween(cjs.Tween.get(this.flameBlue_5).wait(1).to({regX:3.6,regY:3.6,scaleX:2.14,scaleY:2.14,x:35.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:35.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.14,scaleY:2.14,x:35.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:35.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.14,scaleY:2.14,x:35.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:35.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.14,scaleY:2.14,x:35.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:35.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.14,scaleY:2.14,x:35.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:35.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.14,scaleY:2.14,x:35.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:35.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.14,scaleY:2.14,x:35.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:35.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.14,scaleY:2.14,x:35.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:35.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.14,scaleY:2.14,x:35.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:35.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.14,scaleY:2.14,x:35.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:35.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.14,scaleY:2.14,x:35.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:35.5,y:245.5},17,cjs.Ease.get(1)).to({regX:3.6,regY:3.6,scaleX:2.14,scaleY:2.14,x:35.8,y:245.8},11,cjs.Ease.get(-0.4)).to({regX:3.5,regY:3.5,scaleX:1,scaleY:1,x:35.5,y:245.5},17,cjs.Ease.get(1)).wait(27));

	// flamePink
	this.flamePink = new lib.flamePink();
	this.flamePink.setTransform(-11.5,-211.5,1,1,0,0,0,2.5,2.5);

	this.timeline.addTween(cjs.Tween.get(this.flamePink).wait(1).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-211.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-211.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-211.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-211.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-211.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-211.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-211.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-211.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-211.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-211.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-211.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-211.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-211.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-211.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-211.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-211.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-211.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-211.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-211.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-211.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-211.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-211.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-211.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-211.5},17,cjs.Ease.get(1)).wait(27));

	// flamePink
	this.flamePink_1 = new lib.flamePink();
	this.flamePink_1.setTransform(-11.5,-159.5,1,1,0,0,0,2.5,2.5);

	this.timeline.addTween(cjs.Tween.get(this.flamePink_1).wait(1).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-159.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-159.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-159.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-159.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-159.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-159.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-159.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-159.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-159.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-159.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-159.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-159.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-159.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-159.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-159.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-159.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-159.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-159.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-159.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-159.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-159.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-159.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-159.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-159.5},17,cjs.Ease.get(1)).wait(27));

	// flamePink
	this.flamePink_2 = new lib.flamePink();
	this.flamePink_2.setTransform(-11.5,-107.5,1,1,0,0,0,2.5,2.5);

	this.timeline.addTween(cjs.Tween.get(this.flamePink_2).wait(1).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-107.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-107.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-107.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-107.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-107.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-107.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-107.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-107.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-107.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-107.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-107.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-107.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-107.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-107.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-107.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-107.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-107.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-107.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-107.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-107.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-107.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-107.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-107.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-107.5},17,cjs.Ease.get(1)).wait(27));

	// flamePink
	this.flamePink_3 = new lib.flamePink();
	this.flamePink_3.setTransform(-11.5,-55.5,1,1,0,0,0,2.5,2.5);

	this.timeline.addTween(cjs.Tween.get(this.flamePink_3).wait(1).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-55.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-55.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-55.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-55.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-55.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-55.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-55.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-55.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-55.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-55.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-55.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-55.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-55.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-55.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-55.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-55.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-55.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-55.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-55.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-55.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-55.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-55.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:-55.2},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:-55.5},17,cjs.Ease.get(1)).wait(27));

	// flamePink
	this.flamePink_4 = new lib.flamePink();
	this.flamePink_4.setTransform(-11.5,-3.5,1,1,0,0,0,2.5,2.5);

	this.timeline.addTween(cjs.Tween.get(this.flamePink_4).wait(1).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-3.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-3.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-3.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-3.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-3.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-3.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-3.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-3.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-3.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-3.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-3.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-3.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-3.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-3.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-3.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-3.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-3.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-3.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-3.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-3.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-3.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-3.5},17,cjs.Ease.get(1)).to({scaleX:2.4,scaleY:2.4,x:-11.4,y:-3.4},11,cjs.Ease.get(-0.4)).to({scaleX:1,scaleY:1,x:-11.5,y:-3.5},17,cjs.Ease.get(1)).wait(27));

	// flamePink
	this.flamePink_5 = new lib.flamePink();
	this.flamePink_5.setTransform(-11.5,48.5,1,1,0,0,0,2.5,2.5);

	this.timeline.addTween(cjs.Tween.get(this.flamePink_5).wait(1).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:48.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:48.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:48.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:48.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:48.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:48.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:48.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:48.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:48.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:48.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:48.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:48.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:48.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:48.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:48.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:48.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:48.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:48.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:48.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:48.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:48.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:48.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:48.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:48.5},17,cjs.Ease.get(1)).wait(27));

	// flamePink
	this.flamePink_6 = new lib.flamePink();
	this.flamePink_6.setTransform(-11.5,100.5,1,1,0,0,0,2.5,2.5);

	this.timeline.addTween(cjs.Tween.get(this.flamePink_6).wait(1).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:100.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:100.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:100.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:100.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:100.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:100.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:100.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:100.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:100.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:100.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:100.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:100.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:100.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:100.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:100.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:100.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:100.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:100.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:100.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:100.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:100.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:100.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:100.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:100.5},17,cjs.Ease.get(1)).wait(27));

	// flamePink
	this.flamePink_7 = new lib.flamePink();
	this.flamePink_7.setTransform(-11.5,152.5,1,1,0,0,0,2.5,2.5);

	this.timeline.addTween(cjs.Tween.get(this.flamePink_7).wait(1).to({regY:2.6,scaleX:2.42,scaleY:2.42,x:-11.4,y:152.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:152.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.42,scaleY:2.42,x:-11.4,y:152.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:152.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.42,scaleY:2.42,x:-11.4,y:152.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:152.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.42,scaleY:2.42,x:-11.4,y:152.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:152.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.42,scaleY:2.42,x:-11.4,y:152.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:152.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.42,scaleY:2.42,x:-11.4,y:152.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:152.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.42,scaleY:2.42,x:-11.4,y:152.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:152.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.42,scaleY:2.42,x:-11.4,y:152.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:152.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.42,scaleY:2.42,x:-11.4,y:152.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:152.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.42,scaleY:2.42,x:-11.4,y:152.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:152.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.42,scaleY:2.42,x:-11.4,y:152.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:152.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.42,scaleY:2.42,x:-11.4,y:152.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:152.5},17,cjs.Ease.get(1)).wait(27));

	// flamePink
	this.flamePink_8 = new lib.flamePink();
	this.flamePink_8.setTransform(-11.5,204.5,1,1,0,0,0,2.5,2.5);

	this.timeline.addTween(cjs.Tween.get(this.flamePink_8).wait(1).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:204.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:204.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:204.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:204.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:204.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:204.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:204.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:204.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:204.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:204.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:204.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:204.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:204.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:204.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:204.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:204.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:204.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:204.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:204.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:204.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:204.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:204.5},17,cjs.Ease.get(1)).to({regY:2.6,scaleX:2.4,scaleY:2.4,x:-11.4,y:204.8},11,cjs.Ease.get(-0.4)).to({regY:2.5,scaleX:1,scaleY:1,x:-11.5,y:204.5},17,cjs.Ease.get(1)).wait(27));

	// -
	this.shape_558 = new cjs.Shape();
	this.shape_558.graphics.f("rgba(255,255,255,0)").s().p("Eg2XAkUMAAAhIoMBsuAAAMAAABIog");
	this.shape_558.setTransform(348,-1.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_558).wait(364));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14,-233.6,710,482.6);


(lib.main = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// animate
	this.animation = new lib.animation();
	this.animation.setTransform(280.5,350,1,1,0,0,0,280.5,115);

	this.timeline.addTween(cjs.Tween.get(this.animation).wait(1));

	// ___
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(204,51,51,0)").s().p("Eg6WAoxMAAAhRiMB0sAAAMAAABRig");
	this.shape.setTransform(348.5,236);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25,-25,747,522);


// stage content:
(lib.index = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		this.buttonGo.addEventListener("click", go.bind(this));
		this.buttonAway.addEventListener("click", away.bind(this));
		function go() {
			this.main.animation.gotoAndPlay("go");
		}
		function away() {
			this.main.animation.gotoAndPlay("away");
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// btn
	this.buttonGo = new lib.buttonGo();
	this.buttonGo.setTransform(74.8,35,0.259,0.257,0,0,0,158,34.3);

	this.buttonAway = new lib.buttonAway();
	this.buttonAway.setTransform(74.5,51,0.475,0.469,0,0,0,156,32.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.buttonAway},{t:this.buttonGo}]}).wait(1));

	// lines
	this.instance = new lib.lines();
	this.instance.setTransform(395.5,274,1.058,1.062,0,0,0,348.9,-232.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// ---        main
	this.main = new lib.main();
	this.main.setTransform(27.3,27.2,1.058,1.062,0,0,0,0.9,0.7);

	this.timeline.addTween(cjs.Tween.get(this.main).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(378,256,821.9,850);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;