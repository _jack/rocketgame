import VueRouter from 'vue-router'

import CategoryForMen from './pages/CategoryForMen'
import CategoryForWomen from './pages/CategoryForWomen'
import CategoryRandom from './pages/CategoryRandom'
import Form from './components/Form'


export default new VueRouter({
  routes: [
    {
      path: '/',
      component: CategoryForMen,
      props: true
    },
    {
      path: '/for-women',
      component: CategoryForWomen,
      props: true
    },
    {
      path: '/random',
      component: CategoryRandom,
      props: true
    },
    {
      path: '/form',
      component: Form,
      props: true
    },
    {path: '*', redirect: '/'}
  ],
  mode: 'history',

})
