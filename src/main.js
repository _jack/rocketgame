import Vue from 'vue'
import App from './App.vue';

window.Vue = Vue;
import VueRouter from 'vue-router'
import router from './routes'

Vue.use(VueRouter)
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue);
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import './filters.js';
import './main.scss';

var vm = new Vue({
  el: '#app',
  render: h => h(App),
  router
});
Vue.config.devtools = true
